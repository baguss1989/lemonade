﻿using Dapper;
using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class MasterPegawaiDAL
    {
        public IEnumerable<DM_PEGAWAI> GetMasterPegawai()
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT TO_CHAR(REC_ID)REC_ID, NIPP, NAMA, REC_STAT FROM DM_PEGAWAI";
                var result = connection.Query<DM_PEGAWAI>(sql);

                return result;
            }
        }

        public outputModel CrudMasterPegawai(string XAKSI, string XNIPP, string XNAMA, string XREC_STAT)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XNIPP", XNIPP);
                    p.Add("XNAMA", XNAMA);
                    p.Add("XREC_STAT", XREC_STAT);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DM_PEGAWAI", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }
    }
}