﻿using Dapper;
using Lemonade.Entities;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class SettingMenuDAL
    {
        public List<APP_MENU> Menu()
        {
            string query = "";
            List<APP_MENU> AllData = null;
            using (IDbConnection conn = ServiceConfig.GetConnection())
            {
                try
                {
                    query = @"SELECT TO_CHAR(MENU_ID)MENU_ID, MENU_NAME, MENU_CONTROLLER, MENU_ACTION, MENU_ICON, TO_CHAR(MENU_ORDER_BY)MENU_ORDER_BY, MENU_STATUS, TO_CHAR(MENU_PARENT_ID)MENU_PARENT_ID FROM APP_MENU WHERE MENU_STATUS = 'A' ORDER BY MENU_ORDER_BY";

                    AllData = conn.Query<APP_MENU>(query).ToList();
                }
                catch (Exception e)
                {
                    AllData = null;
                }
            }
            return AllData;
        }

        public List<APP_MENU> RoleMenu(string roleId)
        {
            string query = "";
            List<APP_MENU> AllData = null;
            using (IDbConnection conn = ServiceConfig.GetConnection())
            {
                try
                {
                    query = @"SELECT A.* 
                                FROM APP_MENU A, APP_ROLE_MENU B
                                WHERE A.MENU_ID = B.MENU_ID
                                AND B.ROLE_ID = :ROLE_ID
                                AND A.MENU_STATUS = 'A'
                                ORDER BY MENU_ORDER_BY";
                    AllData = conn.Query<APP_MENU>(query,new {
                        ROLE_ID = roleId
                    }).ToList();
                }
                catch (Exception e)
                {
                    AllData = null;
                }
            }
            return AllData;
        }

        public DataTableMenu GetDataTableMenu(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<APP_MENU> listData = null;
            DataTableMenu result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT MENU_ID, MENU_NAME, MENU_CONTROLLER, MENU_ACTION, MENU_ICON, MENU_ORDER_BY, MENU_STATUS, MENU_PARENT_ID 
                                    FROM APP_MENU ORDER BY MENU_ORDER_BY";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<APP_MENU>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT MENU_ID, MENU_NAME, MENU_CONTROLLER, MENU_ACTION, MENU_ICON, MENU_ORDER_BY, MENU_STATUS, MENU_PARENT_ID 
                                    FROM APP_MENU
                                    WHERE UPPER(MENU_NAME||MENU_CONTROLLER||MENU_ACTION||MENU_ICON) LIKE '%' || UPPER(:param) || '%'
                                    ORDER BY MENU_ORDER_BY";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<APP_MENU>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableMenu();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public DataTableRole GetDataTableRole(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<APP_ROLE> listData = null;
            DataTableRole result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT ROLE_ID, ROLE_NAME 
                                    FROM APP_ROLE ORDER BY ROLE_ID";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<APP_ROLE>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT ROLE_ID, ROLE_NAME 
                                    FROM APP_ROLE
                                    WHERE UPPER(ROLE_NAME) LIKE '%' || UPPER(:param) || '%'";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<APP_ROLE>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableRole();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }
    }
}