﻿using Dapper;
using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class AlurDokumenDAL
    {
        public IEnumerable<APP_ALUR> GetAlurDokumen()
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT TO_CHAR(REC_ID)REC_ID, NAMA_ALUR
                                       FROM APP_ALUR
                                       ORDER BY REC_ID";
                var result = connection.Query<APP_ALUR>(sql);

                return result;
            }
        }

        public outputModel crudAlurDokumen(string XAKSI, string XREC_ID, string XNAMA_ALUR)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XREC_ID", XREC_ID);
                    p.Add("XNAMA_ALUR", XNAMA_ALUR);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_APP_ALUR", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }
    }
}