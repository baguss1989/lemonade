﻿using Dapper;
using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class ViewHelperDAL
    {
        public IEnumerable<selectModel> GetKdPelanggan(string xPARAM)
        {
            IEnumerable<selectModel> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"SELECT * FROM VW_PELANGGAN WHERE UPPER(KODE||NAMA) LIKE '%' || UPPER(:PARAM) || '%' ";
                    listData = connection.Query<selectModel>(sql, new { PARAM = xPARAM });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }

        public IEnumerable<selectModel> GetJnsPerjanjian(string xPARAM)
        {
            IEnumerable<selectModel> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"SELECT * FROM VW_JNS_PERJANJIAN WHERE UPPER(KODE||NAMA) LIKE '%' || UPPER(:PARAM) || '%' ";
                    listData = connection.Query<selectModel>(sql, new { PARAM = xPARAM });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }

        public IEnumerable<selectModel> GetPegawai(string xPARAM)
        {
            IEnumerable<selectModel> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"SELECT * FROM VW_PEGAWAI WHERE UPPER(KODE||NAMA) LIKE '%' || UPPER(:PARAM) || '%' ";
                    listData = connection.Query<selectModel>(sql, new { PARAM = xPARAM });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }

        public IEnumerable<selectModel> GetUker(string xPARAM)
        {
            IEnumerable<selectModel> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"SELECT TO_CHAR(REC_ID)KODE, NAMA_UKER NAMA FROM DM_UKER WHERE UPPER(NAMA_UKER) LIKE '%' || UPPER(:PARAM) || '%' ";
                    listData = connection.Query<selectModel>(sql, new { PARAM = xPARAM });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }

        public IEnumerable<selectModel> CariPegawai()
        {
            IEnumerable<selectModel> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"SELECT * FROM VW_PEGAWAI_MDM";
                    listData = connection.Query<selectModel>(sql);
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }

        public IEnumerable<selectModel> GetAlur(string xPARAM)
        {
            IEnumerable<selectModel> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"SELECT REC_ID KODE, NAMA_ALUR NAMA FROM APP_ALUR WHERE UPPER(NAMA_ALUR) LIKE '%' || UPPER(:PARAM) || '%' ";
                    listData = connection.Query<selectModel>(sql, new { PARAM = xPARAM });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }

        public IEnumerable<selectModel> GetRole(string xPARAM, string xUSER_ID)
        {
            IEnumerable<selectModel> listData = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    string sql = @"SELECT * FROM (
                                SELECT A.ROLE_ID KODE,B.ROLE_NAME NAMA
                                FROM APP_USER_ROLE A,APP_ROLE B,APP_USER C
                                WHERE A.ROLE_ID = B.ROLE_ID
                                AND A.USER_ID = C.ID
                                AND C.USERID = :USER_ID
                                ORDER BY 1) WHERE UPPER(NAMA) LIKE '%' || UPPER(:PARAM) || '%' ";
                    listData = connection.Query<selectModel>(sql, new { PARAM = xPARAM, USER_ID = xUSER_ID });
                }
                catch (Exception)
                {
                    listData = null;
                }
            }
            return listData;
        }
    }
}