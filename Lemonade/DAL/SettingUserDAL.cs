﻿using Dapper;
using Lemonade.Entities;
using Lemonade.Helpers;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class SettingUserDAL
    {
        public DataTableUser GetDataTableUser(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<APP_USER> listData = null;
            DataTableUser result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT TO_CHAR(ID)ID,USERID,PASSWD,NAMA,STATUS,CREATED_BY,CREATED_DATE
                                    FROM APP_USER WHERE ID <> 1";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<APP_USER>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT TO_CHAR(ID)ID,USERID,PASSWD,NAMA,STATUS,CREATED_BY,CREATED_DATE
                                    FROM APP_USER
                                    WHERE ID <> 1 AND UPPER(USERID||NAMA) LIKE '%' || UPPER(:param) || '%'";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<APP_USER>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableUser();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public IEnumerable<APP_USER_ROLE> GettableRoleUser(string XUSERID)
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"select a.*,
case
when
(select count(x.role_id)
from app_role x, app_user_role y, app_user z
where x.role_id = y.role_id
and y.user_id = z.id
and x.role_id = a.role_id
and z.userid = :USERID)
> 0 then 'Y' else 'T'
end ada
from app_role a
where a.role_id <> 1
order by 1";
                var result = connection.Query<APP_USER_ROLE>(sql, new
                {
                    USERID = XUSERID
                });

                return result;
            }
        }

        public outputModel crudUSER(string XAKSI, string XUSERID, string XPASSWD, string XNAMA, string XSTATUS)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";
            DecEncrypt crypt = new DecEncrypt();
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XUSERID", XUSERID);
                    p.Add("XPASSWD", crypt.Encrypt(XPASSWD));
                    p.Add("XNAMA", XNAMA);
                    p.Add("XSTATUS", XSTATUS);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_USER", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public outputModel crudUserRole(string XAKSI, string XUSERID, string XROLE_ID)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XUSERID", XUSERID);
                    p.Add("XROLE_ID", XROLE_ID);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_USER_ROLE", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

    }
}