﻿using System;
using System.Collections.Generic;
using Dapper;
using Lemonade.Entities;
using System.Data;

namespace Lemonade.DAL
{
    public class MasterUkerDAL
    {
        public IEnumerable<DM_UKER> GetMasterUker()
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT TO_CHAR(REC_ID)REC_ID, NAMA_UKER, REC_STAT FROM DM_UKER";
                var result = connection.Query<DM_UKER>(sql);

                return result;
            }
        }

        public outputModel CrudMasterUker(string XAKSI, string XREC_ID, string XNAMA_UKER, string XREC_STAT)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XREC_ID", XREC_ID);
                    p.Add("XNAMA_UKER", XNAMA_UKER);
                    p.Add("XREC_STAT", XREC_STAT);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DM_UKER", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }
    }
}