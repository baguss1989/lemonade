﻿using Dapper;
using Lemonade.Entities;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class DashboardDAL
    {
        public dashboardModel GetCounterDashboard()
        {
            string XSTS = "";
            string XMSG = "";

            string sts = "";
            string msg = "";

            string XCOUNT_DRAFT = "";
            string XCOUNT_AKTIF = "";
            string XCOUNT_EXP = "";
            string XCOUNT_ARSIP = "";

            string COUNT_DRAFT = "";
            string COUNT_AKTIF = "";
            string COUNT_EXP = "";
            string COUNT_ARSIP = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var p = new DynamicParameters();
                    p.Add("COUNT_DRAFT", XCOUNT_DRAFT, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("COUNT_AKTIF", XCOUNT_AKTIF, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("COUNT_EXP", XCOUNT_EXP, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("COUNT_ARSIP", XCOUNT_ARSIP, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("GET_COUNT_DASHBOARD", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = "S";
                    COUNT_DRAFT = p.Get<string>("COUNT_DRAFT");
                    COUNT_AKTIF = p.Get<string>("COUNT_AKTIF");
                    COUNT_EXP = p.Get<string>("COUNT_EXP");
                    COUNT_ARSIP = p.Get<string>("COUNT_ARSIP");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                dashboardModel result = new dashboardModel();
                result.COUNT_DRAFT = COUNT_DRAFT;
                result.COUNT_AKTIF = COUNT_AKTIF;
                result.COUNT_EXP = COUNT_EXP;
                result.COUNT_ARSIP = COUNT_ARSIP;
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public summaryModel GetCounterSummary()
        {
            string XSTS = "";
            string XMSG = "";

            string sts = "";
            string msg = "";

            string XCOUNT_KERJASAMA = "";
            string XCOUNT_MOU = "";
            string XCOUNT_PEMBORONGAN = "";
            string XCOUNT_PROPERTI = "";

            string COUNT_KERJASAMA = "";
            string COUNT_MOU = "";
            string COUNT_PEMBORONGAN = "";
            string COUNT_PROPERTI = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var p = new DynamicParameters();
                    p.Add("COUNT_KERJASAMA", XCOUNT_KERJASAMA, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("COUNT_MOU", XCOUNT_MOU, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("COUNT_PEMBORONGAN", XCOUNT_PEMBORONGAN, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("COUNT_PROPERTI", XCOUNT_PROPERTI, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("GET_COUNT_SUMMARY", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = "S";
                    COUNT_KERJASAMA = p.Get<string>("COUNT_KERJASAMA");
                    COUNT_MOU = p.Get<string>("COUNT_MOU");
                    COUNT_PEMBORONGAN = p.Get<string>("COUNT_PEMBORONGAN");
                    COUNT_PROPERTI = p.Get<string>("COUNT_PROPERTI");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                summaryModel result = new summaryModel();
                result.COUNT_KERJASAMA = COUNT_KERJASAMA;
                result.COUNT_MOU = COUNT_MOU;
                result.COUNT_PEMBORONGAN = COUNT_PEMBORONGAN;
                result.COUNT_PROPERTI = COUNT_PROPERTI;
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public DataTableVwPerjanjian GetDataSummary(int draw, int start, int length, string search, string XJNS_PERJANJIAN)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<VW_DT_PERJANJIAN> listData = null;
            DataTableVwPerjanjian result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT * FROM VW_DT_PERJANJIAN WHERE JNS_PERJANJIAN = :JNS";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            JNS = XJNS_PERJANJIAN
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            JNS = XJNS_PERJANJIAN
                        });
                    }
                    else
                    {
                        string sql = @"SELECT *
                                       FROM VW_DT_PERJANJIAN
                                       WHERE JNS_PERJANJIAN = :JNS
                                       AND UPPER(NM_PERJANJIAN||NO_DOKUMEN||JUDUL||DESKRIPSI||TGL_BERLAKU||TGL_BERAKHIR||TGL_PERJANJIAN||PIHAK_TERKAIT) LIKE '%' || UPPER(:param) || '%' ";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search,
                            JNS = XJNS_PERJANJIAN
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search,
                            JNS = XJNS_PERJANJIAN
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableVwPerjanjian();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public outputModel GetCounterDokumenBerlaku()
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var p = new DynamicParameters();
                    p.Add("JML", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("GET_COUNT_DOK_AKTIF", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = "S";
                    msg = p.Get<string>("JML");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public DataTableVwPerjanjian GetDataBerlaku(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<VW_DT_PERJANJIAN> listData = null;
            DataTableVwPerjanjian result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT *
                                FROM VW_DT_PERJANJIAN
                                WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMMDD') >= TO_CHAR (SYSDATE, 'YYYYMMDD')";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT *
                                       FROM VW_DT_PERJANJIAN
                                       WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMMDD') >= TO_CHAR (SYSDATE, 'YYYYMMDD')
                                       AND UPPER(NM_PERJANJIAN||NO_DOKUMEN||JUDUL||DESKRIPSI||TGL_BERLAKU||TGL_BERAKHIR||TGL_PERJANJIAN||PIHAK_TERKAIT) LIKE '%' || UPPER(:param) || '%' ";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableVwPerjanjian();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public outputModel GetCounterDraft()
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var p = new DynamicParameters();
                    p.Add("JML", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("GET_COUNT_DRAFT", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = "S";
                    msg = p.Get<string>("JML");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public DataTableVwPerjanjian GetDataDraft(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<VW_DT_PERJANJIAN> listData = null;
            DataTableVwPerjanjian result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT *
                                FROM VW_DT_PERJANJIAN
                                WHERE STATUS <> 'Selesai'";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT *
                                       FROM VW_DT_PERJANJIAN
                                       WHERE STATUS <> 'Selesai'
                                       AND UPPER(NM_PERJANJIAN||NO_DOKUMEN||JUDUL||DESKRIPSI||TGL_BERLAKU||TGL_BERAKHIR||TGL_PERJANJIAN||PIHAK_TERKAIT) LIKE '%' || UPPER(:param) || '%' ";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableVwPerjanjian();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public outputModel GetCounterDokumenBerakhir()
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var p = new DynamicParameters();
                    p.Add("JML", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("GET_COUNT_DOK_TDK_AKTIF", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = "S";
                    msg = p.Get<string>("JML");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public DataTableVwPerjanjian GetDataBerakhir(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<VW_DT_PERJANJIAN> listData = null;
            DataTableVwPerjanjian result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT *
                                FROM VW_DT_PERJANJIAN
                                WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMMDD') <= TO_CHAR (SYSDATE, 'YYYYMMDD')
                                AND REC_ID NOT IN (SELECT DISTINCT KODE_PERJANJIAN FROM DT_ARSIP)";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT *
                                       FROM VW_DT_PERJANJIAN
                                       WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMMDD') <= TO_CHAR (SYSDATE, 'YYYYMMDD')
                                       AND REC_ID NOT IN (SELECT DISTINCT KODE_PERJANJIAN FROM DT_ARSIP)
                                       AND UPPER(NM_PERJANJIAN||NO_DOKUMEN||JUDUL||DESKRIPSI||TGL_BERLAKU||TGL_BERAKHIR||TGL_PERJANJIAN||PIHAK_TERKAIT) LIKE '%' || UPPER(:param) || '%' ";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableVwPerjanjian();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public DataTableVwPerjanjian GetDataArsip(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<VW_DT_PERJANJIAN> listData = null;
            DataTableVwPerjanjian result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT * FROM VW_DT_PERJANJIAN
                        WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMMDD') <= TO_CHAR (SYSDATE, 'YYYYMMDD')
                        AND REC_ID IN (SELECT DISTINCT KODE_PERJANJIAN FROM DT_ARSIP)";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT * FROM VW_DT_PERJANJIAN
                        WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMMDD') <= TO_CHAR (SYSDATE, 'YYYYMMDD')
                        AND REC_ID IN (SELECT DISTINCT KODE_PERJANJIAN FROM DT_ARSIP)
                        AND UPPER(NM_PERJANJIAN||NO_DOKUMEN||JUDUL||DESKRIPSI||TGL_BERLAKU||TGL_BERAKHIR||TGL_PERJANJIAN||PIHAK_TERKAIT) LIKE '%' || UPPER(:param) || '%' ";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableVwPerjanjian();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public IEnumerable<EXPMONTH_MODEL> GetExpired()
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT * FROM VW_EXP_MONTH WHERE THNBLN >= TO_CHAR(SYSDATE, 'YYYYMM') ORDER BY 1";
                var result = connection.Query<EXPMONTH_MODEL>(sql);

                return result;
            }
        }

        public DataTableVwPerjanjian GetDataExpired(int draw, int start, int length, string search, string XTHNBLN)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<VW_DT_PERJANJIAN> listData = null;
            DataTableVwPerjanjian result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT * FROM VW_DT_PERJANJIAN WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMM') = :yyyymm";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            yyyymm = XTHNBLN
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new {
                            yyyymm = XTHNBLN
                        });
                    }
                    else
                    {
                        string sql = @"SELECT *
                                       FROM VW_DT_PERJANJIAN
                                       WHERE TO_CHAR (TO_DATE (TGL_BERAKHIR, 'DD/MM/YYYY'), 'YYYYMM') = :yyyymm
                                       AND UPPER(NM_PERJANJIAN||NO_DOKUMEN||JUDUL||DESKRIPSI||TGL_BERLAKU||TGL_BERAKHIR||TGL_PERJANJIAN||PIHAK_TERKAIT) LIKE '%' || UPPER(:param) || '%' ";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search,
                            yyyymm = XTHNBLN
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search,
                            yyyymm = XTHNBLN
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableVwPerjanjian();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }
    }
}