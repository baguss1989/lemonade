﻿using Dapper;
using Lemonade.Entities;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class PerjanjianDAL
    {
        public DataTableVwPerjanjian GetDataPerjanjian(int draw, int start, int length, string search)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<VW_DT_PERJANJIAN> listData = null;
            DataTableVwPerjanjian result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT *
                                       FROM VW_DT_PERJANJIAN";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount);
                    }
                    else
                    {
                        string sql = @"SELECT *
                                       FROM VW_DT_PERJANJIAN
                                       WHERE UPPER(REC_ID||NM_PERJANJIAN||NO_DOKUMEN||JUDUL||DESKRIPSI||PIHAK_TERKAIT) LIKE '%' || UPPER(:param) || '%' ";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_DT_PERJANJIAN>(fullSql, new
                        {
                            a = start,
                            b = end,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            param = search
                        });
                    }
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTableVwPerjanjian();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public DataTablePerjanjianPlg GetPelanggan(int draw, int start, int length, string search, string XKODE_PERJANJIAN)
        {
            int count = 0;
            int end = start + length;
            IEnumerable<DT_PERJANJIAN_DTL_PLG> listData = null;
            DataTablePerjanjianPlg result = null;
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string fullSql =
                    "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
                string fullSqlCount = "SELECT count(*) FROM (sql)";
                try
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = @"SELECT TO_CHAR(ID)ID, KODE_PEL, NAMA_PEL, KODE_PERJANJIAN
                                       FROM DT_PERJANJIAN_DTL_PLG
                                       WHERE KODE_PERJANJIAN = :KODE_PERJANJIAN";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<DT_PERJANJIAN_DTL_PLG>(fullSql, new
                        {
                            a = start,
                            b = end,
                            KODE_PERJANJIAN = XKODE_PERJANJIAN
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            KODE_PERJANJIAN = XKODE_PERJANJIAN
                        });
                    }
                    else
                    {
                        string sql = @"SELECT TO_CHAR(ID)ID, KODE_PEL, NAMA_PEL, KODE_PERJANJIAN
                                       FROM DT_PERJANJIAN_DTL_PLG
                                       WHERE KODE_PERJANJIAN = :KODE_PERJANJIAN
                                       AND UPPER(NAMA_PEL) LIKE '%' || UPPER(:param) || '%' ";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<DT_PERJANJIAN_DTL_PLG>(fullSql, new
                        {
                            a = start,
                            b = end,
                            KODE_PERJANJIAN = XKODE_PERJANJIAN,
                            param = search
                        });

                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new
                        {
                            KODE_PERJANJIAN = XKODE_PERJANJIAN,
                            param = search
                        });
                    }                    
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            result = new DataTablePerjanjianPlg();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData;
            return result;
        }

        public IEnumerable<DT_PERJANJIAN_DTL_PLG> GetPlg(string XKODE_PERJANJIAN)
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT TO_CHAR(ID)ID, KODE_PEL, NAMA_PEL, KODE_PERJANJIAN
                                       FROM DT_PERJANJIAN_DTL_PLG
                                       WHERE KODE_PERJANJIAN = :KODE_PERJANJIAN
                                       ORDER BY ID";
                var result = connection.Query<DT_PERJANJIAN_DTL_PLG>(sql, new
                {
                    KODE_PERJANJIAN = XKODE_PERJANJIAN
                });

                return result;
            }
        }

        public IEnumerable<DT_PERJANJIAN_DTL_PIC> GetPIC(string XKODE_PERJANJIAN)
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT TO_CHAR(ID)ID, KODE_PERJANJIAN, KODE_PEG, NAMA_PEG
                                       FROM DT_PERJANJIAN_DTL_PIC
                                       WHERE KODE_PERJANJIAN = :KODE_PERJANJIAN
                                       ORDER BY ID";
                var result = connection.Query<DT_PERJANJIAN_DTL_PIC>(sql, new
                {
                    KODE_PERJANJIAN = XKODE_PERJANJIAN
                });

                return result;
            }
        }

        public IEnumerable<DT_PERJANJIAN_DTL_UKER> GetUKER(string XKODE_PERJANJIAN)
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT KODE_PERJANJIAN, KODE_UKER, NAMA_UKER
                                       FROM DT_PERJANJIAN_DTL_UKER
                                       WHERE KODE_PERJANJIAN = :KODE_PERJANJIAN";
                var result = connection.Query<DT_PERJANJIAN_DTL_UKER>(sql, new
                {
                    KODE_PERJANJIAN = XKODE_PERJANJIAN
                });

                return result;
            }
        }

        public IEnumerable<DT_ATTACHMENT> GetAtt(string XKODE_PERJANJIAN)
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT KODE_PERJANJIAN, FILENAME
                                       FROM DT_ATTACHMENT
                                       WHERE KODE_PERJANJIAN = :KODE_PERJANJIAN";
                var result = connection.Query<DT_ATTACHMENT>(sql, new
                {
                    KODE_PERJANJIAN = XKODE_PERJANJIAN
                });

                return result;
            }
        }

        public IEnumerable<DT_PERJANJIAN_DTL_STAT> GetStat(string XKODE_PERJANJIAN)
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT TO_CHAR(ID)ID,KODE_PERJANJIAN, NAMA_ALUR,REC_STAT,TO_CHAR(CREATED_DATE,'DD-MM-YYYY HH24:MI:SS') CREATED_DATE,KODE_ALUR
                                       FROM DT_PERJANJIAN_DTL_STAT
                                       WHERE KODE_PERJANJIAN = :KODE_PERJANJIAN
                                       ORDER BY CREATED_DATE";
                var result = connection.Query<DT_PERJANJIAN_DTL_STAT>(sql, new
                {
                    KODE_PERJANJIAN = XKODE_PERJANJIAN
                });

                return result;
            }
        }



        public outputModel crudPerjanjian(string XAKSI, string XRECID, string XNO_DOK, string XJUDUL, string XDESKRIPSI, string XTGL1, string XTGL2, string XTGLPERJANJIAN, string XJNS_PERJANJIAN, string XUSER)
        {
            string XSTS = ""; string XMSG = ""; //string XREC_ID = "";
            string sts = ""; string msg = ""; //string rec_id = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XRECID", XRECID);
                    p.Add("XNO_DOK", XNO_DOK);
                    p.Add("XJUDUL", XJUDUL);
                    p.Add("XDESKRIPSI", XDESKRIPSI);
                    p.Add("XTGL1", XTGL1);
                    p.Add("XTGL2", XTGL2);
                    p.Add("XTGLPERJANJIAN", XTGLPERJANJIAN);
                    p.Add("XJNS_PERJANJIAN", XJNS_PERJANJIAN);
                    p.Add("XUSER", XUSER);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    //p.Add("XREC_ID", XREC_ID, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DT_PERJANJIAN", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                    //rec_id = p.Get<string>("XREC_ID");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                //result.REC_ID = rec_id;
                return result;
            }
        }

        public outputModel crudAttachment(string XAKSI, string XKODE_PERJANJIAN, string XFILENAME)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XKODE_PERJANJIAN", XKODE_PERJANJIAN);
                    p.Add("XFILENAME", XFILENAME);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DT_ATTACHMENT", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public outputModel crudPLG(string XAKSI, string XKODE_PERJANJIAN, string XKODE_PEL)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XKODE_PERJANJIAN", XKODE_PERJANJIAN);
                    p.Add("XKODE_PEL", XKODE_PEL);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DT_PERJANJIAN_DTL_PLG", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public outputModel crudPIC(string XAKSI, string XKODE_PERJANJIAN, string XKODE_PEG)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XKODE_PERJANJIAN", XKODE_PERJANJIAN);
                    p.Add("XKODE_PEG", XKODE_PEG);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DT_PERJANJIAN_DTL_PIC", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public outputModel crudUker(string XAKSI, string XKODE_PERJANJIAN, string XKODE_UKER, string XNAMA_UKER)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XKODE_PERJANJIAN", XKODE_PERJANJIAN);
                    p.Add("XKODE_UKER", XKODE_UKER);
                    p.Add("XNAMA_UKER", XNAMA_UKER);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DT_PERJANJIAN_DTL_UKER", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public outputModel crudALUR(string XAKSI, string XID, string XKODE_PERJANJIAN, string XKODE_ALUR, string XREC_STAT)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XID", XID);
                    p.Add("XKODE_PERJANJIAN", XKODE_PERJANJIAN);
                    p.Add("XKODE_ALUR", XKODE_ALUR);
                    p.Add("XREC_STAT", XREC_STAT);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DT_PERJANJIAN_DTL_STAT", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }

        public outputModel crudArsip(string XAKSI, string XKODE_PERJANJIAN)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";

            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XKODE_PERJANJIAN", XKODE_PERJANJIAN);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DT_ARSIP", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }
    }
}