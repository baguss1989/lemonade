﻿using Dapper;
using Lemonade.Entities;
using Lemonade.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class AuthDAL
    {
        public APP_USER getUserInformation(string userId, string passWord)
        {
            APP_USER result = new APP_USER();
            APP_USER_LOKAL getUser = new APP_USER_LOKAL();
            ROLE_MODEL getRole = new ROLE_MODEL();
            try
            {
                getUser = getUserLokal(userId, passWord);
            }
            catch (Exception e)
            {
                getUser.STS = "E";
                getUser.MSG = e.Message;
            }

            try
            {
                getRole = getUserRole(userId);
            }
            catch (Exception e)
            {
                getRole.STS = "E";
                getRole.MSG = e.Message;
            }

            if (getUser.STS == "S" && getRole.STS == "S")
            {
                result.ID = getUser.ID;
                result.USERID = getUser.USERID;
                result.PASSWD = getUser.PASSWD;
                result.NAMA = getUser.NAMA;
                result.STATUS = getUser.STS;
                result.ROLE_ID = getRole.ROLE_ID;
                result.ROLE_NAME = getRole.ROLE_NAME;
            }
            else if(getUser.STS == "S" && getRole.STS == "E")
            {
                result.STATUS = "E";
                result.NAMA = getRole.MSG;
                //result.NAMA = "Login gagal. Hubungi Administrator.";
            }
            else
            {
                result.STATUS = "E";
                //result.NAMA = "Login gagal. Hubungi Administrator.";
                result.NAMA = getUser.MSG;
            }
            return result;
        }
        private APP_USER_LOKAL getUserLokal(string userId, string passWord)
        {
            APP_USER_LOKAL data = new APP_USER_LOKAL();
            DecEncrypt decrpt = new DecEncrypt();
            string psw = decrpt.Encrypt(passWord);
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var sql = @"SELECT TO_CHAR(ID)ID,USERID,PASSWD,NAMA,STATUS,CREATED_BY,CREATED_DATE
                                FROM APP_USER WHERE USERID = :USER_ID AND PASSWD=:PASSWD";
                    data = connection.Query<APP_USER_LOKAL>(sql, new
                    {
                        USER_ID = userId,
                        PASSWD = psw
                    }).Single();
                    //}).FirstOrDefault();

                    
                    if (!string.IsNullOrEmpty(data.NAMA))
                    {
                        if (data.STATUS == "D")
                        {
                            data.STS = "E";
                            data.MSG = "USER TERBLOKIR/TIDAK AKTIF";
                        }
                        else
                        {
                            data.STS = "S";
                            data.MSG = "USER VALID";
                        }
                    }
                    //else if (string.IsNullOrEmpty(data.NAMA))
                    //{
                    //    data.STS = "E";
                    //    data.MSG = "USER ATAU PASSWORD SALAH";
                    //}
                    //else
                    //{
                    //    data.STS = "E";
                    //    data.MSG = "USER TERBLOKIR/TIDAK AKTIF";
                    //}
                }
                catch (Exception e)
                {
                    data.STS = "E";
                    data.MSG = "User ID atau Password anda salah.";
                }
            }
            return data;
        }

        private ROLE_MODEL getUserRole(string userId)
        {
            ROLE_MODEL data = new ROLE_MODEL();
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    var sql = @"SELECT * FROM (
                                SELECT A.ROLE_ID,B.ROLE_NAME
                                FROM APP_USER_ROLE A,APP_ROLE B,APP_USER C
                                WHERE A.ROLE_ID = B.ROLE_ID
                                AND A.USER_ID = C.ID
                                AND C.USERID = :USER_ID
                                ORDER BY 1)
                                WHERE ROWNUM = 1";
                    data = connection.Query<ROLE_MODEL>(sql, new
                    {
                        USER_ID = userId
                    }).Single();

                    if (!string.IsNullOrEmpty(data.ROLE_NAME))
                    {
                        data.STS = "S";
                        data.MSG = "ROLE VALID";
                    }
                }
                catch (Exception e)
                {
                    data.STS = "E";
                    data.MSG = "User tidak mempunyai hak akses pada aplikasi ini.";
                }
            }
            return data;
        }

    }
}