﻿using Dapper;
using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade.DAL
{
    public class MasterPelangganDAL
    {
        public IEnumerable<DM_PELANGGAN> GetMasterPelanggan()
        {
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                string sql = @"SELECT TO_CHAR(REC_ID)REC_ID, KODE_PELANGGAN, NAMA_PELANGGAN, ALAMAT, KOTA, TLP1, TLP2, EMAIL, 
CREATED_BY, TO_CHAR(CREATED_DATE,'DD/MM/YYYY HH24:MI:SS')CREATED_DATE, NVL(UPDATED_BY,'-')UPDATED_BY, NVL(TO_CHAR(UPDATED_DATE,'DD/MM/YYYY HH24:MI:SS'),'-')UPDATED_DATE, REC_STAT
FROM DM_PELANGGAN";
                var result = connection.Query<DM_PELANGGAN>(sql);

                return result;
            }
        }

        public outputModel CrudMasterPelanggan(string XAKSI, string XKODE_PELANGGAN, string XNAMA_PELANGGAN, string XALAMAT, string XKOTA, string XTLP1, string XTLP2, string XEMAIL, string XREC_STAT, string XUSER)
        {
            string XSTS = ""; string XMSG = "";
            string sts = ""; string msg = "";
            using (IDbConnection connection = ServiceConfig.GetConnection())
            {
                try
                {
                    //Insert Header
                    var p = new DynamicParameters();
                    p.Add("XAKSI", XAKSI);
                    p.Add("XKODE_PELANGGAN", XKODE_PELANGGAN);
                    p.Add("XNAMA_PELANGGAN", XNAMA_PELANGGAN);
                    p.Add("XALAMAT", XALAMAT);
                    p.Add("XKOTA", XKOTA);
                    p.Add("XTLP1", XTLP1);
                    p.Add("XTLP2", XTLP2);
                    p.Add("XEMAIL", XEMAIL);
                    p.Add("XREC_STAT", XREC_STAT);
                    p.Add("XUSER", XUSER);
                    p.Add("XSTS", XSTS, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    p.Add("XMSG", XMSG, dbType: DbType.AnsiString, direction: ParameterDirection.Output);
                    connection.Execute("IUD_DM_PELANGGAN", p, null, null, commandType: CommandType.StoredProcedure);

                    sts = p.Get<string>("XSTS");
                    msg = p.Get<string>("XMSG");
                }
                catch (Exception e)
                {
                    sts = "E";
                    msg = e.Message;
                }
                outputModel result = new outputModel();
                result.STS = sts;
                result.MSG = msg;
                return result;
            }
        }
    }
}