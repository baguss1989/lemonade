﻿using Lemonade.DAL;
using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    public class PegawaiController : Controller
    {
        // GET: Pegawai
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult cariPegawai()
        {
            ViewHelperDAL dal = new ViewHelperDAL();
            var result = dal.CariPegawai();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult masterPegawai()
        {
            MasterPegawaiDAL dal = new MasterPegawaiDAL();
            var result = dal.GetMasterPegawai();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MasterPegawaiIUD(string XAKSI, string XNIPP, string XNAMA, string XREC_STAT)
        {
            MasterPegawaiDAL dal = new MasterPegawaiDAL();
            outputModel result = dal.CrudMasterPegawai(XAKSI, XNIPP, XNAMA, XREC_STAT);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}