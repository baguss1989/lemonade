﻿using Lemonade.DAL;
using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    public class PelangganController : Controller
    {
        // GET: Pelanggan
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getmasterPelanggan()
        {
            MasterPelangganDAL dal = new MasterPelangganDAL();
            var result = dal.GetMasterPelanggan();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MasterPelangganIUD(string XAKSI, string XKODE_PELANGGAN, string XNAMA_PELANGGAN, string XALAMAT, string XKOTA, string XTLP1, string XTLP2, string XEMAIL, string XREC_STAT, string XUSER)
        {
            MasterPelangganDAL dal = new MasterPelangganDAL();
            outputModel result = dal.CrudMasterPelanggan(XAKSI,XKODE_PELANGGAN,XNAMA_PELANGGAN,XALAMAT,XKOTA,XTLP1,XTLP2,XEMAIL,XREC_STAT,XUSER);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}