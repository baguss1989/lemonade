﻿using Lemonade.DAL;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Error404()
        {
            ViewBag.Message = "The page you are looking for does not exist; it may have been moved, or removed altogether. You might want to try the search function. Alternatively, return to the front page.";

            return View();
        }

        public ActionResult Error500()
        {
            return View();
        }

        public JsonResult getDashboard()
        {
            DashboardDAL dal = new DashboardDAL();
            var result = dal.GetCounterDashboard();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTableDashboard()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            string XJNS_PERJANJIAN = queryString["XJNS_PERJANJIAN"];
            DataTableVwPerjanjian result = null;
            DashboardDAL dal = new DashboardDAL();
            result = dal.GetDataSummary(draw, start, length, search, XJNS_PERJANJIAN);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSummary()
        {
            DashboardDAL dal = new DashboardDAL();
            var result = dal.GetCounterSummary();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTableSummary()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            string XJNS_PERJANJIAN = queryString["XJNS_PERJANJIAN"];
            DataTableVwPerjanjian result = null;
            DashboardDAL dal = new DashboardDAL();
            result = dal.GetDataSummary(draw, start, length, search, XJNS_PERJANJIAN);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getExpired()
        {
            DashboardDAL dal = new DashboardDAL();
            var result = dal.GetExpired();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTableExpired()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            string XTHNBLN = queryString["XTHNBLN"];
            DataTableVwPerjanjian result = null;
            DashboardDAL dal = new DashboardDAL();
            result = dal.GetDataExpired(draw, start, length, search, XTHNBLN);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCounterBerlaku()
        {
            DashboardDAL dal = new DashboardDAL();
            var result = dal.GetCounterDokumenBerlaku();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTableBerlaku()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableVwPerjanjian result = null;
            DashboardDAL dal = new DashboardDAL();
            result = dal.GetDataBerlaku(draw, start, length, search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCounterDraft()
        {
            DashboardDAL dal = new DashboardDAL();
            var result = dal.GetCounterDraft();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTableDraft()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableVwPerjanjian result = null;
            DashboardDAL dal = new DashboardDAL();
            result = dal.GetDataDraft(draw, start, length, search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCounterBerakhir()
        {
            DashboardDAL dal = new DashboardDAL();
            var result = dal.GetCounterDokumenBerakhir();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTableBerakhir()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableVwPerjanjian result = null;
            DashboardDAL dal = new DashboardDAL();
            result = dal.GetDataBerakhir(draw, start, length, search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTableArsip()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableVwPerjanjian result = null;
            DashboardDAL dal = new DashboardDAL();
            result = dal.GetDataArsip(draw, start, length, search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}