﻿using Lemonade.DAL;
using Lemonade.Entities;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        // GET: Auth

        [HttpGet]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                var claimsIdentity = User.Identity as System.Security.Claims.ClaimsIdentity;
                return RedirectToAction(claimsIdentity.FindFirst("URL_HOME").Value, "Home");
            }
            else
            {
                //ViewBag.ReturnUrl = returnUrl;
                return View();
            }
            //return View();
        }

        public JsonResult AuthLogin(string UserId, string Passwd)
        {
            string xsts = "";
            string xmsg = "";
            //string url = "";
            try
            {
                AuthDAL dal = new AuthDAL();
                APP_USER user = dal.getUserInformation(UserId, Passwd);

                if (user.STATUS == "S")
                {
                    xmsg = "/";

                    var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, user.NAMA),
                        new Claim("USERID", user.USERID),
                        new Claim("STATUS", user.STATUS),
                        new Claim("ROLE_ID", user.ROLE_ID),
                        new Claim("ROLE_NAME", user.ROLE_NAME)
                    }, "ApplicationCookie");

                    var ctx = Request.GetOwinContext();
                    var authManager = ctx.Authentication;
                    authManager.SignIn(identity);
                }
                else
                {
                    xsts = "E";
                    //xmsg = "Login Gagal.";
                    xmsg = user.NAMA;
                }
            }
            catch (Exception e)
            {
                xsts = "E";
                xmsg = e.Message;
            }
            outputModel ret = new outputModel();
            ret.STS = xsts;
            ret.MSG = xmsg;
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Home");
        }

        public JsonResult getUserRole(string p,string u)
        {
            IEnumerable<selectModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetRole(p,u);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UbahRole(string xRoleId, string xRoleName)
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            var identity = new ClaimsIdentity(User.Identity);
            identity.RemoveClaim(identity.FindFirst("ROLE_ID"));
            identity.AddClaim(new Claim("ROLE_ID", xRoleId));
            identity.RemoveClaim(identity.FindFirst("ROLE_NAME"));
            identity.AddClaim(new Claim("ROLE_NAME", xRoleName));
            authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties { IsPersistent = true });
            var result = "/";// identity.FindFirst("URL_HOME").Value;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}