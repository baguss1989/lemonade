﻿using Lemonade.DAL;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    public class SettingMenuController : Controller
    {
        // GET: SettingMenu
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getDatatableMenu()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableMenu result = null;
            SettingMenuDAL dal = new SettingMenuDAL();
            result = dal.GetDataTableMenu(draw, start, length, search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDatatableRole()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableRole result = null;
            SettingMenuDAL dal = new SettingMenuDAL();
            result = dal.GetDataTableRole(draw, start, length, search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
    }
}