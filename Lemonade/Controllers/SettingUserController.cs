﻿using Lemonade.DAL;
using Lemonade.Entities;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    public class SettingUserController : Controller
    {
        // GET: SettingUser
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getDatatableUser()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableUser result = null;
            SettingUserDAL dal = new SettingUserDAL();
            result = dal.GetDataTableUser(draw, start, length, search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult userIUD(string XAKSI, string XUSERID, string XPASSWD, string XNAMA, string XSTATUS)
        {
            SettingUserDAL dal = new SettingUserDAL();
            outputModel result = dal.crudUSER(XAKSI, XUSERID, XPASSWD, XNAMA, XSTATUS);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult userroleIUD(string XAKSI, string XUSERID, string XROLE_ID)
        {
            SettingUserDAL dal = new SettingUserDAL();
            outputModel result = dal.crudUserRole(XAKSI, XUSERID, XROLE_ID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult gettableRoleUser(string XUSERID)
        {
            SettingUserDAL dal = new SettingUserDAL();
            var result = dal.GettableRoleUser(XUSERID);
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

    }
}