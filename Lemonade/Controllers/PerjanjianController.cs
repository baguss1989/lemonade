﻿using Lemonade.DAL;
using Lemonade.Entities;
using Lemonade.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace Lemonade.Controllers
{
    public class PerjanjianController : Controller
    {
        // GET: Perjanjian
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult All()
        {
            return View();
        }

        public JsonResult getKdPelanggan(string p)
        {
            IEnumerable<selectModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetKdPelanggan(p);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getJnsPerjanjian(string p)
        {
            IEnumerable<selectModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetJnsPerjanjian(p);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPegawai(string p)
        {
            IEnumerable<selectModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetPegawai(p);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSelectUker(string p)
        {
            IEnumerable<selectModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetUker(p);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSelectAlur(string p)
        {
            IEnumerable<selectModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetAlur(p);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDatatablePerjanjian()
        {
            var queryString = Request.QueryString;
            int draw = int.Parse(queryString["draw"]);
            int start = int.Parse(queryString["start"]);
            int length = int.Parse(queryString["length"]);
            string search = queryString["search"];
            DataTableVwPerjanjian result = null;
            PerjanjianDAL dal = new PerjanjianDAL();
            result = dal.GetDataPerjanjian(draw, start, length,search);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPlg(string xkode_perjanjian)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            var result = dal.GetPlg(xkode_perjanjian);
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPIC(string xkode_perjanjian)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            var result = dal.GetPIC(xkode_perjanjian);
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUker(string xkode_perjanjian)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            var result = dal.GetUKER(xkode_perjanjian);
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getAtt(string xkode_perjanjian)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            var result = dal.GetAtt(xkode_perjanjian);
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getStat(string xkode_perjanjian)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            var result = dal.GetStat(xkode_perjanjian);
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult perjanjianIUD(string XAksi, string xRECID, string xNO_DOK, string xJUDUL, string XDESKRIPSI, string xTGL1, string xTGL2, string XTGLPERJANJIAN, string XJNS_PERJANJIAN)
        {
            var XUSER = User.Identity.Name.ToString();// CurrentUser.UserId.ToString();
            PerjanjianDAL dal = new PerjanjianDAL();
            outputModel result = dal.crudPerjanjian(XAksi, xRECID, xNO_DOK, xJUDUL, XDESKRIPSI, xTGL1, xTGL2, XTGLPERJANJIAN, XJNS_PERJANJIAN, XUSER);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult plgIUD(string XAksi, string XKODE_PERJANJIAN, string XKODE_PEL)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            outputModel result = dal.crudPLG(XAksi, XKODE_PERJANJIAN, XKODE_PEL);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult picIUD(string XAksi, string XKODE_PERJANJIAN, string XKODE_PEG)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            outputModel result = dal.crudPIC(XAksi, XKODE_PERJANJIAN, XKODE_PEG);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ukerIUD(string XAksi, string XKODE_PERJANJIAN, string XKODE_UKER, string XNAMA_UKER)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            outputModel result = dal.crudUker(XAksi, XKODE_PERJANJIAN, XKODE_UKER, XNAMA_UKER);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult alurIUD(string XAKSI, string XID, string XKODE_PERJANJIAN, string XKODE_ALUR, string XREC_STAT)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            outputModel result = dal.crudALUR(XAKSI, XID, XKODE_PERJANJIAN, XKODE_ALUR, XREC_STAT);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult attachmentIUD(string XAksi, string XKODE_PERJANJIAN, string XFILENAME)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            outputModel result = dal.crudAttachment(XAksi, XKODE_PERJANJIAN, XFILENAME);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult arsipIUD(string XAksi, string XKODE_PERJANJIAN)
        {
            PerjanjianDAL dal = new PerjanjianDAL();
            outputModel result = dal.crudArsip(XAksi, XKODE_PERJANJIAN);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ContentResult UploadFiles()
        {
            var r = new List<UploadFilesResult>();
            //var r = new List<DT_ATTACHMENT>();
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;
                var sFile = Path.GetFileName(hpf.FileName);
                //var sFile = DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(hpf.FileName);
                string savedFileName = Path.Combine(Server.MapPath("~/FileUpload/"), sFile);
                hpf.SaveAs(savedFileName);
                r.Add(new UploadFilesResult()
                {
                    Name = sFile
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\"}", "application/json");
        }

        public JsonResult CekFiles(string fileName)
        {
            string sts = "";
            string msg = "";
            var fleName = Path.Combine(Server.MapPath("~/FileUpload/") + fileName);
            
            try
            {
                if (System.IO.File.Exists(fleName))
                {
                    sts = "S";
                    msg = "File exists";

                    
                }
                else
                {
                    sts = "E";
                    msg = "File not found.";
                }

            }
            catch (Exception e)
            {
                sts = "E";
                msg = e.Message;
            }
            outputModel result = new outputModel();
            result.STS = sts;
            result.MSG = msg;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteFiles(string fileName)
        {
            string sts = "";
            string msg = "";
            var fleName = Path.Combine(Server.MapPath("~/FileUpload/") + fileName);
            try
            {
                if (System.IO.File.Exists(fleName))
                {
                    System.IO.File.Delete(fleName);
                    sts = "S";
                    msg = "Lampiran telah dihapus.";
                }
                else
                {
                    sts = "E";
                    msg = "Lampiran telah dihapus. ";
                }

            }
            catch (Exception e)
            {
                sts = "E";
                msg = e.Message;
            }
            outputModel result = new outputModel();
            result.STS = sts;
            result.MSG = msg;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[WebMethod]
        //public void DeleteFiles(string sFile)
        //{
        //    var fileName = Path.Combine(Server.MapPath("~/FileUpload/") + sFile);
            
        //    if (System.IO.Directory.Exists(fileName))
        //    {
        //        foreach (var item in System.IO.Directory.GetFiles(fileName))
        //        {
        //            System.IO.File.SetAttributes(item, FileAttributes.Normal);
        //            System.IO.File.Delete(item);
        //        }
        //    }
        //}



        
    }
}