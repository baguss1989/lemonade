﻿using Lemonade.DAL;
using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    public class AlurDokumenController : Controller
    {
        // GET: AlurDokumen
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult alurdokumenIUD(string XAksi, string XREC_ID, string XNAMA_ALUR)
        {
            AlurDokumenDAL dal = new AlurDokumenDAL();
            outputModel result = dal.crudAlurDokumen(XAksi, XREC_ID, XNAMA_ALUR);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getAlurDokumen()
        {
            AlurDokumenDAL dal = new AlurDokumenDAL();
            var result = dal.GetAlurDokumen();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        
    }
}