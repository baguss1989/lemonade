﻿using Lemonade.DAL;
using Lemonade.Entities;
using System.Web.Mvc;

namespace Lemonade.Controllers
{
    public class UnitKerjaController : Controller
    {
        // GET: UnitKerja
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult DataTableMasterUker()
        {
            MasterUkerDAL dal = new MasterUkerDAL();
            var result = dal.GetMasterUker();
            return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MasterUkerIUD(string XAKSI, string XREC_ID, string XNAMA_UKER, string XREC_STAT)
        {
            MasterUkerDAL dal = new MasterUkerDAL();
            outputModel result = dal.CrudMasterUker(XAKSI, XREC_ID, XNAMA_UKER, XREC_STAT);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}