﻿var Index = function () {

    $('#btn-lihatdata').click(function () {
        initTableUker();
        lookup();
        hapus();
        $('#modalData').modal({ backdrop: 'static', keyboard: false });
    });

    var initTableUker = function () {
        $('#tableUker').DataTable({
            "ajax": {
                "url": "/UnitKerja/DataTableMasterUker",
                "type": "GET",
                "data": function (data) {

                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NAMA_UKER", "name":"NAMA_UKER"
                },
                {
                    "data": "REC_STAT", "name": "REC_STAT",
                    "render": function (data, type, full, meta) {
                        var sts = '<span class="badge badge-pill badge-' + ((data === 'A') ? "success" : "danger") + '">' + ((data === 'A') ? "AKTIF" : "TIDAK AKTIF") + '</span>';
                        return sts;
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-edit"><i class="icmn-pencil" aria-hidden="true"></i> Edit</a>' +
                            '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": true,
            "bLengthChange": true,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "filter": false
        });
    }

    var lookup = function () {
        $('#tableUker').on('click', '#btn-edit', function (e) {
            var xTable = $('#tableUker').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var stat = "";
            var a = "AKTIF";
            var d = "TIDAK AKTIF";
            if (x["REC_STAT"] === "A") {
                stat = a;
            } else {
                stat = d;
            }
            $('#inputRECID').val(x["REC_ID"]);
            $('#inputNamaUker').val(x["NAMA_UKER"]);
            $('#inputStatus').append("<option value='" + x["REC_STAT"] + "' selected>" + stat + "</option>");
            //$('#inputStatus').val(x["REC_STAT"]);
            $('#modalData').modal('hide');
        });
    }


    $('#btn-simpan').click(function () {
        simpan();
    });

    var simpan = function () {
        var RecId = $('#inputRECID').val();
        var Uker = $('#inputNamaUker').val();
        var Status = $('#inputStatus').val();
        if (Uker) {
            if (RecId > 0) {
                Aksi = "U";
            } else {
                Aksi = "I";
            }
            var param = {
                XAKSI: Aksi,
                XREC_ID: RecId,
                XNAMA_UKER: Uker,
                XREC_STAT: Status
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/UnitKerja/MasterUkerIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                    swal({
                        title: "Error !",
                        text: "Pesan error : " + data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                } else {
                    $('#inputRECID').val(data.STS);
                    swal({
                        title: "Berhasil !",
                        text: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    });
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Anda belum mengisikan data dengan lengkap.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    var hapus = function () {
        $('#tableUker').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tableUker').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            swal({
                title: "Konfirmasi !",
                text: "Anda akan data  " + x["NAMA_UKER"],
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, remove it",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        var RecId = x["REC_ID"];
                        var Uker = x["NAMA_UKER"];
                        var Status = x["REC_STAT"];
                        if (Uker) {
                            var param = {
                                XAKSI: "D",
                                XREC_ID: RecId,
                                XNAMA_UKER: Uker,
                                XREC_STAT: Status
                            };
                            var req = $.ajax({
                                contentType: "application/json",
                                data: JSON.stringify(param),
                                method: "POST",
                                url: "/UnitKerja/MasterUkerIUD",
                                timeout: 30000
                            });
                            req.done(function (data) {
                                if (data.STS === 'E') {
                                    console.log(data.MSG);
                                    swal({
                                        title: "Error !",
                                        text: "Pesan error : " + data.MSG,
                                        type: "error",
                                        confirmButtonClass: "btn-danger",
                                        confirmButtonText: "Cancel"
                                    });
                                } else {
                                    resetform();
                                    initTableUker();
                                    swal({
                                        title: "Berhasil !",
                                        text: data.MSG,
                                        type: "success",
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: "Oke"
                                    });
                                }
                            });
                        } else {
                            swal({
                                title: "Peringatan !",
                                text: "Anda belum mengisikan data dengan lengkap.",
                                type: "warning",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cancel"
                            });
                        }
                    } else {
                        swal({
                            title: "Cancelled",
                            text: "User " + x["USERID"] + "batal dihapus.",
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ya"
                        });
                    }
                });
        });
    }

    $('#btn-bersihkan').click(function () {
        resetform();
    });

    var resetform = function () {
        $('#inputRECID').val('');
        $('#inputNamaUker').val('');
        $('#inputStatus').val('');
    }



    return {
        init: function () {
            

        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});