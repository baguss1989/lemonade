﻿var Index = function () {

    $('#btn-update').hide();

    $('#btnLihatData').click(function () {
        initTableUser();
        $('#modalData').modal({ backdrop: 'static', keyboard: false });
        hapusUser();
    });

    $('#btn-batal').click(function () {
        resetform();
    });

    var resetform = function () {
        $('#btn-update').hide();
        $('#btn-simpan').show();
        $('#inputUserId').val('');
        $('#inputPassword').val('');
        $('#inputNama').val('');
        $('#inputMenuStatus').val('');
        var inputuser = document.getElementById("inputUserId");
        inputuser.removeAttribute("readonly", "");
        inittableRoleUser();
        inittableRoleMaster();
    }

    
    var initTableUser = function () {
        $('#tableUser').DataTable({
            "ajax": {
                "url": "/SettingUser/getDatatableUser",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFind').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "USERID"
                },
                {
                    "data": "NAMA"
                },
                {
                    "data": "STATUS", "name": "STATUS",
                    "render": function (data, type, full, meta) {
                        var sts = '<span class="badge badge-pill badge-' + ((data === 'A') ? "success" : "danger") + '">' + ((data === 'A') ? "AKTIF" : "TIDAK AKTIF") + '</span>';
                        return sts;
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-resetpassword"><i class="icmn-key" aria-hidden="true"></i> Reset Password</a>' +
                            '<a class="btn btn-sm btn-link" id="btn-edit"><i class="icmn-pencil" aria-hidden="true"></i> Edit</a>' +
                            //'<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            //"iDisplayLength": 20,
            //"sDom": "<'table-responsive't><'row'<p i>>",
            //responsive: true,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "filter": false//,
            //    "columnDefs": [
            //        { "width": "20%", "targets": 2 }
            //]
        });

        var table = $('#tableUser').DataTable();
        $('#txtFind').keypress(function () {
            if (event.keyCode == 13) {
                $("#tableUser_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }

    var btnsimpan = function () {
        $('#btn-simpan').click(function () {
            simpan();
            initTableUser();
        });
    }

    var simpan = function () {
        var Aksi = "I";
        var Userid = $('#inputUserId').val();
        var Passwd = $('#inputPassword').val();
        var Nama = $('#inputNama').val();
        var Status = $('#inputMenuStatus').val();
        if (Userid && Passwd && Nama && Status) {
            var param = {
                XAKSI: Aksi,
                XUSERID: Userid,
                XPASSWD: Passwd,
                XNAMA: Nama,
                XSTATUS: Status
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/SettingUser/userIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    console.log(data.MSG);
                    swal({
                        title: "Berhasil !",
                        text: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    });
                    initTableUser();
                    
                } else {
                    console.log(data.MSG);
                    swal({
                        title: "Error !",
                        text: "Pesan error : " + data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Anda belum mengisikan data dengan lengkap.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    var lookup = function () {
        $('#tableUser').on('click', '#btn-edit', function (e) {
            var xTable = $('#tableUser').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var inputuser = document.getElementById("inputUserId");
            inputuser.setAttribute("readonly", "");
            $('#inputUserId').val(x["USERID"]);
            $('#inputNama').val(x["NAMA"]);
            $('#inputPassword').val(x["PASSWD"]);
            $('#inputMenuStatus').val(x["STATUS"]);
            $('#btn-update').show();
            $('#btn-simpan').hide();
            $('#modalData').modal('hide');
            btnupdate();
            inittableRoleUser();
            //inittableRoleMaster();
            //hapusRole();
            //tambahRole();
        });
    }

    var btnupdate = function () {
        $('#btn-update').click(function () {
            edit();
        });
    }

    var edit = function () {
        var xAksi = "U";
        var Userid = $('#inputUserId').val();
        var Passwd = "";//$('#inputPassword').val();
        var Nama = $('#inputNama').val();
        var Status = $('#inputMenuStatus').val();
        if (Userid && Nama && Status) {
            var param = {
                XAKSI: xAksi,
                XUSERID: Userid,
                XPASSWD: Passwd,
                XNAMA: Nama,
                XSTATUS: Status
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/SettingUser/userIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                    swal({
                        title: data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                } else {
                    swal({
                        title: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                initTableUser();
                                resetform();
                            }
                        });
                    console.log(data.MSG);
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Data harus diisi.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }


    var resetpassword = function () {
        $('#tableUser').on('click', '#btn-resetpassword', function (e) {
            var xTable = $('#tableUser').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            $('#displayUserId').val(x["USERID"]);
            $('#modalResetPassword').modal({ backdrop: 'static', keyboard: false });
            btnreset();
        });
    }

    var btnreset = function () {
        $('#btn-reset').click(function () {
            resetpass();
        });
    }

    var resetpass = function () {
        var xAksi = "P";
        var Userid = $('#displayUserId').val();
        var Passwd = $('#inputPasswordBaru').val();
        var Nama = "";
        var Status = "";
        if (Userid && Passwd) {
            var param = {
                XAKSI: xAksi,
                XUSERID: Userid,
                XPASSWD: Passwd,
                XNAMA: Nama,
                XSTATUS: Status
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/SettingUser/userIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                    swal({
                        title: data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                } else {
                    swal({
                        title: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                initTableUser();
                                resetform();
                            }
                        });
                    console.log(data.MSG);
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Data harus diisi.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }


    var hapusUser = function () {
        $('#tableUser').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tableUser').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            swal({
                title: "Konfirmasi !",
                text: "Anda akan menghapus user  " + x["USERID"],
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, remove it",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        var dataD = {
                            XAKSI: "D",
                            XID: x["ID"],
                            XUSERID: x["USERID"],
                            XPASSWD: "",
                            XNAMA: "",
                            XSTATUS: ""
                        }
                        var req = $.ajax({
                            contentType: "application/json",
                            method: "POST",
                            data: JSON.stringify(dataD),
                            url: "/SettingUser/userIUD"
                        });
                        req.done(function (data) {
                            if (data.STS === 'S') {
                                swal({
                                    title: data.MSG,
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "Oke"
                                },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            initTableUser();
                                            resetform();
                                        }
                                    });
                                console.log(data.MSG);
                            } else {
                                swal({
                                    title: data.MSG,
                                    type: "error",
                                    confirmButtonClass: "btn-danger"
                                });
                                console.log(data.MSG);
                            }
                        })
                            .fail(function () {
                                swal({
                                    title: data.MSG,
                                    type: "error",
                                    confirmButtonClass: "btn-danger"
                                });
                                console.log(data.MSG);
                            });
                    } else {
                        swal({
                            title: "Cancelled",
                            text: "User " + x["USERID"] + "batal dihapus.",
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ya"
                        });
                    }
                });
        });
    }


    var inittableRoleUser = function () {
        $('#tableRoleUser').DataTable({
            "ajax": {
                "url": "/SettingUser/gettableRoleUser",
                "type": "GET",
                "data": function (data) {
                    data.XUSERID = $('#inputUserId').val();
                }
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "ROLE_NAME", "name": "ROLE_NAME",
                    "render": function (data, type, full) {
                        return '<span>' + full["ROLE_NAME"] + '</span>';
                    }
                },
                {
                    "data": "ADA", "name": "ADA",
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="' + ((data === 'T') ? "btn-tambahrole" : "btn-hapusrole") + '"><i class="' + ((data === 'T') ? "icmn-plus" : "icmn-bin") + '" aria-hidden="true"></i> ' + ((data === 'T') ? "Tambah" : "Hapus") +'</a>' +
                            '</div></div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                //"zeroRecords": "User tidak memiliki role akses."
                "zeroRecords": '<a class="btn btn-sm btn-link" id="btn-tambahrole"><i class="icmn-plus" aria-hidden="true"></i> Tambah Role</a>'
            },
            "filter": false
        });
    }

    var tambahRole = function () {
        $('#tableRoleUser').on('click', '#btn-tambahrole', function (e) {
            var xTable = $('#tableRoleUser').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var Userid = $('#inputUserId').val();
            if (Userid) {
                var p = {
                    XAKSI: "I",
                    XUSERID: Userid,
                    XROLE_ID: x["ROLE_ID"]
                }
                var req = $.ajax({
                    contentType: "application/json",
                    method: "POST",
                    data: JSON.stringify(p),
                    url: "/SettingUser/userroleIUD"
                });
                req.done(function (data) {
                    if (data.STS === 'S') {
                        swal({
                            title: data.MSG,
                            type: "success",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Oke"
                        },
                            function (isConfirm) {
                                if (isConfirm) {
                                    inittableRoleUser();
                                    //inittableRoleMaster();
                                }
                            });
                        console.log(data.MSG);
                    } else {
                        swal({
                            title: data.MSG,
                            type: "error",
                            confirmButtonClass: "btn-danger"
                        });
                        console.log(data.MSG);
                    }
                });
            } else {
                swal({
                    title: "Peringatan !",
                    text: "Anda belum mengisikan data dengan lengkap.",
                    type: "warning",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Cancel"
                });
            }
        });
    }


    var btntambahrole = function () {
        $('#btn-tambahrole').click(function () {
            simpan();
            inittableRoleMaster();
            tambahRole();
        });
    }

    var inittableRoleMaster = function () {
        $('#tableRoleMaster').DataTable({
            "ajax": {
                "url": "/SettingUser/gettableRoleMaster",
                "type": "GET",
                "data": function (data) {
                    data.XUSERID = $('#inputUserId').val();
                }
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "ROLE_NAME", "name": "ROLE_NAME",
                    "render": function (data, type, full) {
                        return '<span>' + full["ROLE_NAME"] + '</span>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-tambah"><i class="icmn-plus" aria-hidden="true"></i> Tambahkan</a>' +
                            '</div></div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": ""
            },
            "filter": false
        });
    }

    var hapusRole = function () {
        $('#tableRoleUser').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tableRoleUser').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var p = {
                XAKSI: "D",
                XUSERID: x["ID"],
                XROLE_ID: x["ROLE_ID"]
            }
            var req = $.ajax({
                contentType: "application/json",
                method: "POST",
                data: JSON.stringify(p),
                url: "/SettingUser/userroleIUD"
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    swal({
                        title: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                inittableRoleUser();
                                inittableRoleMaster();
                            }
                        });
                    console.log(data.MSG);
                } else {
                    swal({
                        title: data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger"
                    });
                    console.log(data.MSG);
                }
            });
        });
    }

    
    

    return {
        init: function () {
            initTableUser();
            btnsimpan();
            lookup();
            resetpassword();
            inittableRoleUser();
            //inittableRoleMaster();
            
        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});