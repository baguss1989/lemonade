﻿var Index = function () {

    $('#btnLihatData').click(function () {
        initTableMenu();
        $('#modalMenu').modal({ backdrop: 'static', keyboard: false });
    });

    var initTableMenu = function () {
        $('#tableMenu').DataTable({
            "ajax": {
                "url": "/SettingMenu/getDatatableMenu",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFind').val();
                }//,
                //"beforeSend": function () {
                //    plp.blok();
                //},
                //"complete": function () {
                //    plp.unblok();
                //}
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "MENU_ORDER_BY"
                },
                {
                    "data": "MENU_ID"
                },
                {
                    "data": "MENU_NAME"
                },
                {
                    "data": "MENU_CONTROLLER"
                },
                {
                    "data": "MENU_ACTION"
                },
                {
                    "data": "MENU_ICON", "name": "MENU_ICON",
                    "render": function (data, type, full, meta) {
                        var icon = '<i class="' + full["MENU_ICON"] + '" aria-hidden="true"></i>';
                        return icon;
                    }
                },
                {
                    "data": "MENU_PARENT_ID"
                },
                {
                    "data": "MENU_STATUS", "name": "MENU_STATUS",
                    "render": function (data, type, full, meta) {
                        var sts = '<span class="badge badge-pill badge-' + ((data === 'A') ? "success" : "danger") + '">' + ((data === 'A') ? "AKTIF" : "TIDAK AKTIF") + '</span>';
                        return sts;
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-edit"><i class="icmn-pencil" aria-hidden="true"></i> Edit</a>' +
                            '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            //"iDisplayLength": 20,
            //"sDom": "<'table-responsive't><'row'<p i>>",
            //responsive: true,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "filter": false//,
            //    "columnDefs": [
            //        { "width": "20%", "targets": 2 }
            //]
        });

        var table = $('#tableMenu').DataTable();
        $('#txtFind').keypress(function () {
            if (event.keyCode == 13) {
                $("#tableMenu_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }

    var simpan = function () {
        apps.blok();
        var xUserName = $('#NIPP').val();
        var xPassword = $('#Password').val();
        if (xUserName && xPassword) {
            var param = {
                NIPP: xUserName,
                Password: xPassword
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Auth/AuthLogin",
                timeout: 30000
            });
            req.done(function (data) {
                apps.unblok();
                if (data.sts == 'E') {
                    $('body').pgNotification({
                        style: 'flip',
                        message: '<b>Informasi</b><br />' + data.msg,
                        position: 'top-right',
                        timeout: 2000,
                        type: 'error'
                    }).show();
                } else {
                    window.location.href = '/Home';
                }
            });
        } else {
            apps.unblok();
            swal('Peringatan', 'Anda belum mengisikan data dengan lengkap.', 'warning');
        }
    }


    // Start Role
    var initTableRole = function () {
        $('#tableRole').DataTable({
            "ajax": {
                "url": "/SettingMenu/getDatatableRole",
                "type": "GET",
                "data": function (data) {
                    //data.search = $('#txtFindRole').val();
                }
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "ROLE_NAME"
                },
                {
                    "render": function (data, type, full) {
                        //var aksi = '<button id="btn-edit" type="button" class="btn btn-default btn-sm"><i class="icmn-pencil" aria-hidden="true"></i> Edit</button>';
                        var aksi = '<div class="pull-right">'+
                            '<div class="btn-group mr-2 mb-2" aria-label="" role = "group" > '+
                            '<button type = "button" class="btn btn-default btn-sm"><i class="icmn-pencil" aria-hidden="true"></i> Edit</button>' +
                            '<button type = "button" class="btn btn-default btn-sm"><i class="icmn-search" aria-hidden="true"></i> Lihat Menu</button>' +
                            '</div></div> ';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            //"iDisplayLength": 20,
            //"sDom": "<'table-responsive't><'row'<p i>>",
            //responsive: true,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "filter": false,
                "columnDefs": [
                    { "width": "10%", "targets": 1 }
            ]
        });

        var table = $('#tableRole').DataTable();
        $('#txtFindRole').keypress(function () {
            if (event.keyCode == 13) {
                $("#tableRole_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }

    var btnsimpanRole = function () {
        $('#btn-simpan').click(function () {
            simpan();
            initTableUser();
        });
    }

    var simpanRole = function () {
        var Aksi = "I";
        var Userid = $('#inputUserId').val();
        var Passwd = $('#inputPassword').val();
        var Nama = $('#inputNama').val();
        var Status = $('#inputMenuStatus').val();
        if (Userid && Passwd && Nama && Status) {
            var param = {
                XAKSI: Aksi,
                XUSERID: Userid,
                XPASSWD: Passwd,
                XNAMA: Nama,
                XSTATUS: Status
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/SettingUser/userIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    console.log(data.MSG);
                    swal({
                        title: "Berhasil !",
                        text: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    });
                    initTableUser();

                } else {
                    console.log(data.MSG);
                    swal({
                        title: "Error !",
                        text: "Pesan error : " + data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Anda belum mengisikan data dengan lengkap.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    var data = function () {
        $.ajax({
            url: "/SettingMenu/getDatatableMenu",
            type: "GET",
            success: function (result) {
                $("#listmenu").html(result);
                //$('#divRole').html('<ul>' + endMenu.join('') + '</ul>');
            }
        });
    }

    //var data = [
    //    {
    //        "id": "1",
    //        "name": "name_1",
    //        "parent_id": "0"
    //    },
    //    {
    //        "id": "2",
    //        "name": "name_2",
    //        "parent_id": "0"
    //    },
    //    {
    //        "id": "3",
    //        "name": "name_3",
    //        "parent_id": "0"
    //    },
    //    {
    //        "id": "4",
    //        "name": "name_4",
    //        "parent_id": "1"
    //    }
    //];

    //var endMenu = getMenu("0");

    //function getMenu(parentID) {

    //    return data.filter(function (node) { return (node.parent_id === parentID); }).sort(function (a, b) { return a.index > b.index }).map(function (node) {
    //        var exists = data.some(function (childNode) { return childNode.parent_id === node.id; });
    //        var subMenu = (exists) ? '<ul>' + getMenu(node.id).join('') + '</ul>' : "";
    //        return '<li>' + node.name + subMenu + '</li>';
    //    });
    //}
    //$('#divRole').html('<ul>' + endMenu.join('') + '</ul>');

    // End Role
    return {
        init: function () {
            initTableMenu();
            initTableRole();
            //data();
            //btnLogin();
            //etrPassword();
        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});