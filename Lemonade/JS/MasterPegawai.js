﻿var Index = function () {

    $('#btn-lihatdata').click(function () {
        initMasterPegawai();
        $('#modalData').modal({ backdrop: 'static', keyboard: false });
        //hapusUser();
    });

    $('#btn-cari').click(function () {
        initCariPegawai();
        $('#modalCariPegawai').modal({ backdrop: 'static', keyboard: false });
    });

    var initCariPegawai = function () {
        $('#tableCariPegawai').DataTable({
            "ajax": {
                "url": "/Pegawai/cariPegawai",
                "type": "GET",
                "data": function (data) {

                }
            },
            "columns": [
                {
                    "data": "KODE"
                },
                {
                    "data": "NAMA"
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<a class="btn btn-sm" id="btn-pilih"><i class="dropdown-icon icmn-pencil" aria-hidden="true"></i> Pilih</a>';
                        return aksi;
                    }
                }
            ],
            //"serverSide": true,
            "processing": true,
            "destroy": true
        });
    }

    $('#tableCariPegawai').on('click', '#btn-pilih', function (e) {
        var xTable = $('#tableCariPegawai').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        $('#inputNIPP').val(x["KODE"]);
        $('#inputNama').val(x["NAMA"]);
        $('#modalCariPegawai').modal('hide');
    });

    var initMasterPegawai = function () {
        $('#tablePegawai').DataTable({
            "ajax": {
                "url": "/Pegawai/masterPegawai",
                "type": "GET",
                "data": function (data) {

                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NIPP"
                },
                {
                    "data": "NAMA"
                },
                {
                    "data": "REC_STAT", "name": "REC_STAT",
                    "render": function (data, type, full, meta) {
                        var sts = '<span class="badge badge-pill badge-' + ((data === 'A') ? "success" : "danger") + '">' + ((data === 'A') ? "AKTIF" : "TIDAK AKTIF") + '</span>';
                        return sts;
                    }
                },
                {
                    "render": function (data, type, full) {
                        //var aksi = '<div class="dropdown">' +
                        //    '<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
                        //    'Actions' +
                        //    '</button>' +
                        //    '<div class="dropdown-menu" role="menu">' +
                        //    '<a class="dropdown-item" id="btn-edit"><i class="dropdown-icon icmn-pencil" aria-hidden="true"></i> Edit</a>' +
                        //    '<div class="dropdown-divider"></div>' +
                        //    '<a class="dropdown-item" id="btn-disable"><i class="dropdown-icon icmn-hammer" aria-hidden="true"></i> Disable</a>' +
                        //    '</div>' +
                        //    '</div>';
                        var aksi = '<a class="btn btn-sm" id="btn-edit"><i class="icmn-pencil" aria-hidden="true"></i> Edit</a>';
                        return aksi;
                    }
                }
            ],
            //"serverSide": true,
            "processing": true,
            "destroy": true
        });
    }

    $('#sAksi').val("I");

    var btnsimpan = function () {
        $('#btn-simpan').click(function () {
            simpan();
        });
    }

    var simpan = function () {
        var Nipp = $('#inputNIPP').val();
        var Nama = $('#inputNama').val();
        var xAksi = $('#sAksi').val();;
        if (Nama) {
            //if (Nipp !== null) {
            //    xAksi = "U";
            //} else {
            //    xAksi = "I";
            //}
            var param = {
                XAKSI: xAksi,
                XNIPP: Nipp,
                XNAMA: Nama,
                XREC_STAT: $('#inputStatus').val()
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Pegawai/MasterPegawaiIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                    swal({
                        title: "Error !",
                        text: "Pesan error : " + data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });

                } else {
                    console.log(data.MSG);
                    $('#inputNIPP').val(data.STS);
                    swal({
                        title: "Berhasil !",
                        text: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    });
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Nama Pegawai harus diisi.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    $('#btn-bersihkan').click(function () {
        resetform();
    });

    var resetform = function () {
        $('#inputNIPP').val('');
        $('#inputNama').val('');
        $('#sAksi').val("I");
    }

    $('#tablePegawai').on('click', '#btn-edit', function (e) {
        var xTable = $('#tablePegawai').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        $('#inputNIPP').val(x["NIPP"]);
        $('#inputNama').val(x["NAMA"]);
        $('#sAksi').val("U");
        $('#modalData').modal('hide');
    });

    $('#tablePegawai').on('click', '#btn-hapus', function (e) {
        var xTable = $('#tablePegawai').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        swal({
            title: "Konfirmasi !",
            text: "Anda akan menghapus data  " + x["NAMA_PELANGGAN"] + " (" + x["KODE_PELANGGAN"] + ")",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, remove it",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    var dataD = {
                        XAKSI: "D",
                        XKODE_PELANGGAN: x["KODE_PELANGGAN"],
                        XNAMA_PELANGGAN: "",
                        XALAMAT: "",
                        XKOTA: "",
                        XTLP1: "",
                        XTLP2: "",
                        XEMAIL: "",
                        XREC_STAT: "",
                        XUSER: ""
                    }
                    var req = $.ajax({
                        contentType: "application/json",
                        method: "POST",
                        data: JSON.stringify(dataD),
                        url: "/Pelanggan/MasterPelangganIUD"
                    });
                    req.done(function (data) {
                        if (data.STS === 'S') {
                            swal({
                                title: data.MSG,
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Oke"
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        initMasterPelanggan();
                                        resetform();
                                    }
                                });
                            console.log(data.MSG);
                        } else {
                            swal({
                                title: data.MSG,
                                type: "error",
                                confirmButtonClass: "btn-danger"
                            });
                            console.log(data.MSG);
                        }
                    })
                        .fail(function () {
                            swal({
                                title: data.MSG,
                                type: "error",
                                confirmButtonClass: "btn-danger"
                            });
                            console.log(data.MSG);
                        });
                } else {
                    swal({
                        title: "Cancelled",
                        text: "Pelanggan " + x["NAMA_PELANGGAN"] + "batal dihapus.",
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ya"
                    });
                }
            });
    });

    return {
        init: function () {
            //initMasterPelanggan();
            btnsimpan();
        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});