﻿var index = function () {
    //Dashboard
    var getDashboard = function () {
        var req = null;
        req = $.ajax({
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify(),
            method: "POST",
            url: "/Home/getDashboard",
            timeout: 30000
        });
        req.done(function (data) {
            if (data.STS === 'S') {
                //console.log(data); 
                $("#badgeDraft").text(data["COUNT_DRAFT"]);
                $("#badgeBerlaku").text(data["COUNT_AKTIF"]);
                $("#badgeBerakhir").text(data["COUNT_EXP"]);
                $("#badgeArsip").text(data["COUNT_ARSIP"]);
            }
            else {
                console.log(data);
            }
        });
    }


    // Summary
    var getSummary = function () {
        var req = null;
        req = $.ajax({
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify(),
            method: "POST",
            url: "/Home/getSummary",
            timeout: 30000
        });
        req.done(function (data) {
            if (data.STS === 'S') {
                //console.log(data); 
                $("#countKerjasama").text(data["COUNT_KERJASAMA"]);
                $("#countMOU").text(data["COUNT_MOU"]);
                $("#countPemborongan").text(data["COUNT_PEMBORONGAN"]);
                $("#countProperti").text(data["COUNT_PROPERTI"]);
            }
            else {
                console.log(data);  
            }
        });
    }

    var getDataSummary1 = function () {
        $('#sum1').click(function () {
            $('#modalTitleHome').text('Kategori : KERJASAMA');
            $('#paramsum').val("1");
            $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });   
            inittableSummary();
            lihatData();
        });
    }

    var getDataSummary2 = function () {
        $('#sum2').click(function () {
            $('#modalTitleHome').text('Kategori : MOU');
            $('#paramsum').val("2");
            $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
            inittableSummary();
            lihatData();
        });
    }

    var getDataSummary3 = function () {
        $('#sum3').click(function () {
            $('#modalTitleHome').text('Kategori : PEMBORONGAN');
            $('#paramsum').val("3");
            $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
            inittableSummary();
            lihatData();
        });
    }

    var getDataSummary4 = function () {
        $('#sum4').click(function () {
            $('#modalTitle').text('Kategori : PROPERTI');
            $('#paramsum').val("4");
            $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
            inittableSummary();
            lihatData();
        });
    }

    var inittableSummary = function () {
        $('#tableSearchHome').DataTable({
            "ajax": {
                "url": "/Home/getTableSummary",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFindHome').val();
                    data.XJNS_PERJANJIAN = $('#paramsum').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn_lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 3,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearchHome').DataTable();
        $('#txtFindHome').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearchHome_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }

    // Expired Soon Card
    var inittableExpired = function () {
        $('#tableExpired').DataTable({
            "ajax": {
                "url": "/Home/getExpired",
                "type": "GET",
                "data": function (data) {
                    
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "EXP_MONTH", "name": "EXP_MONTH",
                    "render": function (data, type, full) {
                        return '<strong>' + full["EXP_MONTH"] + '</strong>';
                    }
                },
                {
                    "data": "JML", "name": "JML",
                    "render": function (data, type, full) {
                        return '<div class="text-right"><span>' + full["JML"] + '</span></div>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-details"><i class="icmn-search" aria-hidden="true"></i> Details</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": true,
            "bLengthChange": false,
            "paging": true,
            "iDisplayLength": 5,
            "destroy": true,
            "processing": true,
            "info": false
        });
    }

    var expdetail = function () {
        $('#tableExpired').on('click', '#btn-details', function (e) {
            var xTable = $('#tableExpired').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);

            
            $('#modalTitleHome').text('Expired date : ' + x["EXP_MONTH"]);
            $('#thnbln').val(x["THNBLN"]);
            $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
            inittableExpiredDetail();
            lihatData();
        });
    }


    var inittableExpiredDetail = function () {
        $('#tableSearchHome').DataTable({
            "ajax": {
                "url": "/Home/getTableExpired",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFindHome').val();
                    data.XTHNBLN = $('#thnbln').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn_lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 3,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearchHome').DataTable();
        $('#txtFindHome').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearchHome_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }



    //var getDraft = function () {
    //    var req = null;
    //    req = $.ajax({
    //        contentType: "application/json",
    //        traditional: true,
    //        data: JSON.stringify(),
    //        method: "POST",
    //        url: "/Home/getCounterDraft",
    //        timeout: 30000
    //    });
    //    req.done(function (data) {
    //        $("#badgeDraft").text(data.MSG);
    //    });
    //}

    $('#badgeDraft, #divDraft').click(function () {
        inittableDraft();
        lihatData();
        $('#modalTitleHome').text('Draft');
        $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
    });

    var inittableDraft = function () {
        $('#tableSearchHome').DataTable({
            "ajax": {
                "url": "/Home/getTableDraft",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFindHome').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn_lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 3,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearchHome').DataTable();
        $('#txtFindHome').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearchHome_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }



    //var getBerakhir = function () {
    //    var req = null;
    //    req = $.ajax({
    //        contentType: "application/json",
    //        traditional: true,
    //        data: JSON.stringify(),
    //        method: "POST",
    //        url: "/Home/getCounterBerakhir",
    //        timeout: 30000
    //    });
    //    req.done(function (data) {
    //        $("#badgeBerakhir").text(data.MSG);
    //    });
    //}

    $('#badgeBerakhir, #divBerakhir').click(function () {
        inittableBerakhir();
        lihatData();
        $('#modalTitleHome').text('Document Expired');
        $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
    });

    var inittableBerakhir = function () {
        $('#tableSearchHome').DataTable({
            "ajax": {
                "url": "/Home/getTableBerakhir",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFindHome').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn_lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 3,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearchHome').DataTable();
        $('#txtFindHome').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearchHome_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }




    //var getBerlaku = function () {
    //    var req = null;
    //    req = $.ajax({
    //        contentType: "application/json",
    //        traditional: true,
    //        data: JSON.stringify(),
    //        method: "POST",
    //        url: "/Home/getCounterBerlaku",
    //        timeout: 30000
    //    });
    //    req.done(function (data) {
    //        $("#badgeBerlaku").text(data.MSG);
    //    });
    //}

    $('#badgeBerlaku, #divBerlaku').click(function () {
        inittableBerlaku();
        lihatData();
        $('#modalTitleHome').text('Document Active');
        $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
    });

    var inittableBerlaku = function () {
        $('#tableSearchHome').DataTable({
            "ajax": {
                "url": "/Home/getTableBerlaku",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFindHome').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn_lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 3,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearchHome').DataTable();
        $('#txtFindHome').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearchHome_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }

    //var getArsip = function () {
    //    var req = null;
    //    req = $.ajax({
    //        contentType: "application/json",
    //        traditional: true,
    //        data: JSON.stringify(),
    //        method: "POST",
    //        url: "/Home/getCounterArsip",
    //        timeout: 30000
    //    });
    //    req.done(function (data) {
    //        $("#badgeArsip").text(data.MSG);
    //    });
    //}

    $('#badgeArsip, #divArsip').click(function () {
        inittableArsip();
        lihatData();
        $('#modalTitleHome').text('Document Archive');
        $('#modalSearchHome').modal({ backdrop: 'static', keyboard: false });
    });

    var inittableArsip = function () {
        $('#tableSearchHome').DataTable({
            "ajax": {
                "url": "/Home/getTableArsip",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFindHome').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn_lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 3,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearchHome').DataTable();
        $('#txtFindHome').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearchHome_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }



    
    var lihatData = function () {
        $('#tableSearchHome').on('click', '#btn_lihat', function (e) {
            var xTable = $('#tableSearchHome').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);

            $('#inputNoRec').val(x["REC_ID"]);

            $('#NomorDokumen').text(x["NO_DOKUMEN"]);
            $('#JudulDokumen').text(x["JUDUL"]);
            $('#JnsPerjanjian').text(x["NM_PERJANJIAN"]);
            $('#TglPerjanjian').text(x["TGL_PERJANJIAN"]);
            $('#Deskripsi').text(x["DESKRIPSI"]);
            $('#TglBerlaku').text(x["TGL_BERLAKU"]);
            $('#TglBerakhir').text(x["TGL_BERAKHIR"]);

            $('#created_by').text(x["CREATED_BY"]);
            $('#created_date').text(x["CREATED_DATE"]);
            $('#updated_by').text(x["UPDATED_BY"]);
            $('#updated_date').text(x["UPDATED_DATE"]);

            $("#tab1").trigger('click');
            $('#modalLihatDataHome').modal({ backdrop: 'static', keyboard: false });
            initTablePelanggan();
            initTableAttachment();
            initTablePIC();
            initTableUker();
            initTableAlur();
        });
    }

    var initTablePelanggan = function () {
        $('#tablePelanggan').DataTable({
            "ajax": {
                "url": "/Perjanjian/getPlg",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                {
                    "data": "NAMA_PEL", "name": "NAMA_PEL",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_PEL"] + '</span>';
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Pihak Terkait tidak ditemukan."
            },
            "filter": false
        });
    }

    var initTableAttachment = function () {
        $('#tableAttachment').DataTable({
            "ajax": {
                "url": "/Perjanjian/getAtt",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                {
                    "data": "FILENAME", "name": "FILENAME",
                    "render": function (data, type, full) {
                        return '<small>' + full["FILENAME"] + '</small>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-download"><i class="icmn-download" aria-hidden="true"></i> Download</a>' +
                            //'<a href="http://lemonade.pelindo.co.id/FileUpload/' + full["FILENAME"] + '" target="_blank" class="btn btn-sm btn-link" id="btn-download" download><i class="icmn-download" aria-hidden="true"></i> Download</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Belum ada Lampiran."
            },
            "filter": false
        });
    }

    $('#tableAttachment').on('click', '#btn-download', function (e) {
        var xTable = $('#tableAttachment').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        var sfile = x["FILENAME"];
        var cek = $.ajax({
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify({ fileName: sfile }),
            url: "/Perjanjian/CekFiles"
        });
        cek.done(function (data) {
            if (data.STS === 'S') {
                swal({
                    title: "Download file ?",
                    text: "<strong>" + sfile + "</strong>  </br></br>  <a href='FileUpload/" + sfile + "' target='_blank'><button class='btn btn-success'>DOWNLOAD FILE</button></a>",
                    html: true,
                    type: "warning",
                    showConfirmButton: false
                });
            } else {
                swal({
                    title: data.MSG,
                    text: "Lampiran tidak ditemukan. Mohon hubungi Admin untuk mengunggah ulang lampiran tersebut.",
                    type: "error",
                    confirmButtonText: "Oke !",
                    confirmButtonClass: "btn-secondary"
                });
            }
        });
    });

    var initTablePIC = function () {
        $('#tablePIC').DataTable({
            "ajax": {
                "url": "/Perjanjian/getPIC",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                {
                    "data": "NAMA_PEG", "name": "NAMA_PEG",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_PEG"] + '</span>';
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Belum ada PIC."
            },
            "filter": false
        });
    }

    var initTableUker = function () {
        $('#tableUker').DataTable({
            "ajax": {
                "url": "/Perjanjian/getUker",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                {
                    "data": "NAMA_UKER", "name": "NAMA_UKER",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_UKER"] + '</span>';
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Belum ada Dinas Terkait."
            },
            "filter": false
        });
    }

    var initTableAlur = function () {
        $('#tableAlur').DataTable({
            "ajax": {
                "url": "/Perjanjian/getStat",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NAMA_ALUR", "name": "NAMA_ALUR",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_ALUR"] + '</span>';
                    }
                },
                {
                    "data": "REC_STAT", "name": "REC_STAT",
                    "render": function (data, type, full) {
                        return '<span class="badge badge-pill badge-' + ((data === '1') ? "success" : "info") + '">' + ((data === '1') ? "Selesai" : "Dalam Proses") + '</span><br/>' +
                            '<small class="text-muted">Updated: ' + full["CREATED_DATE"] + '</small><div class="pull-right">';
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Status dokumen tidak ditemukan."
            },
            "filter": false
        });
    }

    
    return {
        init: function () {
            getDashboard();
            getSummary();
            getDataSummary1();
            getDataSummary2();
            getDataSummary3();
            getDataSummary4();
            inittableExpired();
            expdetail();

            //getDraft();
            //getBerakhir();
            //getBerlaku();
            //getArsip();
        }
    };
}();

function clearsearch() {
    $('#txtFindHome').val("");
}

jQuery(document).ready(function () {
    index.init();
});