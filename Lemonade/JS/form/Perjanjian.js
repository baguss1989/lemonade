﻿var Index = function () {
    var myForm = $("#formInput");
    var nStatus = false;
    var nEditing = null;
    var nNew = false;
    var look = 0;
    var xAksi = "I";

    var buttonArsip = function () {
        $("#btnArsip").click(function () {
            var kode_perjanjian = $("#inputNoRec").val();
            if (kode_perjanjian) {
                swal({
                    title: "Apakah dokumen ini akan diarsipkan ?",
                    text: $("#inputJudul").val() +
                        "</br></br>" +
                        "<button id='btnPindahArsip' type='button' class='btn btn-lg btn-success'><i class='icmn-enter' aria-hidden='true' ></i> Pindahkan</button >",
                    html: true,
                    showConfirmButton: false,
                    showCancelButton: true,
                    cancelButtonText: "Cancel"
                });

                $("#btnPindahArsip").click(function () {
                    var xAksi = "I";
                    var param = {
                        XAKSI: xAksi,
                        XKODE_PERJANJIAN: kode_perjanjian
                    };
                    var req = $.ajax({
                        contentType: "application/json",
                        data: JSON.stringify(param),
                        method: "POST",
                        url: "/Perjanjian/arsipIUD",
                        timeout: 30000
                    });
                    req.done(function (data) {
                        if (data.STS === 'S') {
                            $.notify({
                                title: '<strong>Success ! </strong>',
                                message: data.MSG
                            }, {
                                type: 'success'
                            });

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000); 

                        } else {
                            $.notify({
                                title: '<strong>Error ! </strong>',
                                message: data.MSG
                            }, {
                                type: 'danger'
                            });
                        }
                    });
                });
            }
            else {
                swal({
                    title: "Error!",
                    text: "Tidak ada dokumen yang dipilih.",
                    type: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Oke !",
                    confirmButtonClass: "btn btn-secondary"
                });
            }
        });

        

    }





    
    // BEGIN Tambah Pelanggan
    $("#divPlg").hide();

    var btnAddpelanggan = function () {
        $('#btnTambahPelanggan').click(function () {
            simpanHeadersilent();
            simpanPelanggan();
            hapusPelanggan();
        });
    }

    var select2pelanggan = function () {
        $("#inputPelanggan").select2({
            ajax: {
                url: "/Perjanjian/getKdPelanggan",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.NAMA + ' (' + item.KODE + ')',
                                id: item.KODE,
                                r_text: item.NAMA
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1
            //minimumInputLength: 0 // untuk menampilkan semua
        });
    }

    var initTablePelanggan = function () {
        $('#tablePelanggan').DataTable({
            "ajax": {
                "url": "/Perjanjian/getPlg",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "NAMA_PEL", "name": "NAMA_PEL",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_PEL"] + '</span>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        //var aksi = '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": ""
            },
            "filter": false
        });
    }

    var simpanPelanggan = function () {
        var xAksi = "I";
        var KodePerjanjian = $('#inputNoRec').val();
        var KodePel = $('#inputPelanggan').val();
        if (KodePerjanjian && KodePel) {
            var param = {
                XAKSI: xAksi,
                XKODE_PERJANJIAN: KodePerjanjian,
                XKODE_PEL: KodePel
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Perjanjian/plgIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                } else {
                    console.log(data.MSG);
                    initTablePelanggan();
                }
            });
        } 
    }

    var hapusPelanggan = function () {
        $('#tablePelanggan').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tablePelanggan').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var dataD = {
                XAKSI: "D",
                XKODE_PERJANJIAN: x["KODE_PERJANJIAN"],
                XKODE_PEL: x["KODE_PEL"]
            }
            var req = $.ajax({
                contentType: "application/json",
                method: "POST",
                data: JSON.stringify(dataD),
                url: "/Perjanjian/plgIUD"
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    console.log(data.MSG);
                    initTablePelanggan();
                } else {
                    console.log(data.MSG);
                }
            })
                .fail(function () {
                    console.log(data.MSG);
                });
        });
    }
    // END Tambah Pelanggan




    // BEGIN Tambah PIC
    $("#divPIC").hide();

    var btnAddPIC = function () {
        $('#btnTambahPIC').click(function () {
            $("#divPIC").show();
            select2pegawai();
            simpanPIC();
            initTablePIC();
            hapusPIC();
            $("#inputPIC").val('');
        });
    }

    var select2pegawai = function () {
        $("#inputPIC").select2({
            ajax: {
                url: "/Perjanjian/getPegawai",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.NAMA + ' (' + item.KODE + ')',
                                id: item.KODE,
                                r_text: item.NAMA
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1
            //minimumInputLength: 0 // untuk menampilkan semua
        });
    }

    var simpanPIC = function () {
        var xAksi = "I";
        var KodePerjanjian = $('#inputNoRec').val();
        var KodePeg = $('#inputPIC').val();
        if (KodePerjanjian && KodePeg) {
            var param = {
                XAKSI: xAksi,
                XKODE_PERJANJIAN: KodePerjanjian,
                XKODE_PEG: KodePeg
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Perjanjian/picIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                } else {
                    console.log(data.MSG);
                    select2pegawai();
                    initTablePIC();
                }
            });
        }
    }

    var initTablePIC = function () {
        $('#tablePIC').DataTable({
            "ajax": {
                "url": "/Perjanjian/getPIC",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "NAMA_PEG", "name": "NAMA_PEG",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_PEG"] + '</span>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        //var aksi = '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": ""
            },
            "filter": false,
            "columnDefs": [
                { "width": "15%", "targets": 1 }
            ]
        });
    }

    var hapusPIC = function () {
        $('#tablePIC').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tablePIC').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var dataD = {
                XAKSI: "D",
                XKODE_PERJANJIAN: x["KODE_PERJANJIAN"],
                XKODE_PEG: x["KODE_PEG"]
            }
            var req = $.ajax({
                contentType: "application/json",
                method: "POST",
                data: JSON.stringify(dataD),
                url: "/Perjanjian/picIUD"
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    console.log(data.MSG);
                    initTablePIC();
                } else {
                    console.log(data.MSG);
                }
            })
                .fail(function () {
                    console.log(data.MSG);
                });
        });
    }
    // END Tambah PIC



    // BEGIN Tambah Uker
    $("#divUker").hide();

    $('#btnTambahUker').click(function () {
        $("#divUker").show();
        select2Uker();
        simpanUker();
        initTableUker();
        hapusUker();
        $("#inputUker").val('');
    });

    var select2Uker = function () {
        $("#inputUker").select2({
            ajax: {
                url: "/Perjanjian/getSelectUker",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.NAMA,// + ' (' + item.KODE + ')',
                                id: item.KODE,
                                r_text: item.NAMA
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1
            //minimumInputLength: 0 // untuk menampilkan semua
        });
    }

    var simpanUker = function () {
        var xAksi = "I";
        var KodePerjanjian = $('#inputNoRec').val();
        var KodeUker = $('#inputUker').val();
        var NamaUker = $('#inputUker').text();
        if (KodePerjanjian && KodeUker) {
            var param = {
                XAKSI: xAksi,
                XKODE_PERJANJIAN: KodePerjanjian,
                XKODE_UKER: KodeUker,
                XNAMA_UKER: NamaUker
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Perjanjian/ukerIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                } else {
                    console.log(data.MSG);
                    select2Uker();
                    initTableUker();
                }
            });
        }
    }

    var initTableUker = function () {
        $('#tableUker').DataTable({
            "ajax": {
                "url": "/Perjanjian/getUker",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "NAMA_UKER", "name": "NAMA_UKER",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_UKER"] + '</span>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        //var aksi = '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": ""
            },
            "filter": false,
            "columnDefs": [
                { "width": "15%", "targets": 1 }
            ]
        });
    }

    var hapusUker = function () {
        $('#tableUker').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tableUker').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var dataD = {
                XAKSI: "D",
                XKODE_PERJANJIAN: x["KODE_PERJANJIAN"],
                XKODE_UKER: x["KODE_UKER"]
            }
            var req = $.ajax({
                contentType: "application/json",
                method: "POST",
                data: JSON.stringify(dataD),
                url: "/Perjanjian/ukerIUD"
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    console.log(data.MSG);
                    initTableUker();
                } else {
                    console.log(data.MSG);
                }
            })
                .fail(function () {
                    console.log(data.MSG);
                });
        });
    }
    // END Tambah Uker



    // BEGIN Attachment
    var initTableAttachment = function () {
        $('#tableAttachment').DataTable({
            "ajax": {
                "url": "/Perjanjian/getAtt",
                "type": "GET",
                "data": function (data) {
                    //data.search = $('#txtFind').val();
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                //{
                //    "render": function (data, type, full, meta) {
                //        return meta.row + meta.settings._iDisplayStart + 1;
                //    }
                //},
                {
                    "data": "FILENAME", "name": "FILENAME",
                    "render": function (data, type, full) {
                        return '<small>' + full["FILENAME"] + '</small>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            //'<a href="~/FileUpload/' + full["FILENAME"] +'" target="_blank" class="btn btn-sm btn-link" id="btn-download"><i class="icmn-download" aria-hidden="true"></i> Download</a>' +
                            '<a class="btn btn-sm btn-link" id="btn-download"><i class="icmn-download" aria-hidden="true"></i> Download</a>' +
                            '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Belum ada Lampiran."
            },
            "filter": false
        });

    }

    $('#btn_att').click(function () {
        swal({
            title: "Pilih lampiran",
            text: "<input class='btn' type='file' id='inputFile' name='file' hidden>" +
                "<input type='text' class='form-control' id='inputFileName' placeholder='Choose File' readonly>" +
                "<br /> <br /> <br /> " +
                "<button id='btnUpload' type='button' class='btn btn-lg btn-success'><i class='icmn-attachment' aria-hidden='true' ></i>Upload</button >",
            html: true,
            showConfirmButton: false,
            showCancelButton: true,
            cancelButtonText: "Cancel Upload"
        });

        $('#inputFileName').click(function () {
            var RecID = $('#inputNoRec').val();
            var FileName = $('#inputFileName').val();
            if (RecID) {
                if (FileName) {
                    swal({
                        title: "Lampiran akan direset!",
                        text: FileName,
                        //type: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Oke !",
                        confirmButtonClass: "btn btn-secondary"
                    });
                } else {
                    $("#inputFile").trigger('click');
                }

            } else {
                swal({
                    title: "Error!",
                    text: "ID Perjanjian tidak ditemukan.",
                    type: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Oke !",
                    confirmButtonClass: "btn btn-secondary"
                });
            }
        });


        $("#inputFile").change(function (e) {
            var NoRec = $('#inputNoRec').val();
            //var NoDok = $('#inputNomorDok').val();
            //var NoDoc = NoDok.replace(/\//g, "_");
            $('#inputFileName').val(NoRec + '-' + e.target.files[0].name);
            var fSize = e.target.files[0].size / 1024 / 1024;
            var fileSize = Math.round(fSize).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var maxSize = 50;
            if (fileSize >= maxSize) {
                swal({
                    title: "File to large!",
                    text: "Max size: " + maxSize + " MB.",
                    type: "warning",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-primary",
                    confirmButtonText: "Oke !"
                });
            }
        });

        $('#inputFile').fileupload({
            dataType: 'json',
            url: '/Perjanjian/UploadFiles',
            add: function (e, data) {
                var NoRec = $('#inputNoRec').val();
                var count = data.files.length;
                var i;
                for (i = 0; i < count; i++) {
                    data.files[i].uploadName = NoRec + '-' + data.files[i].name;
                    var sfile = data.files[i].uploadName;
                    var upload = data;
                }
                $('#btnUpload').click(function () {
                    var Rec_ID = $('#inputNoRec').val();
                    if (Rec_ID) {
                        var param = {
                            XAKSI: "I",
                            XKODE_PERJANJIAN: $('#inputNoRec').val(),
                            XFILENAME: sfile
                        };
                        var req = $.ajax({
                            contentType: "application/json",
                            method: "POST",
                            data: JSON.stringify(param),
                            url: "/Perjanjian/attachmentIUD"
                        });
                        req.done(function (data) {
                            if (data.STS === 'E') {
                                swal({
                                    title: "Error!",
                                    text: data.MSG,
                                    type: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Oke !",
                                    confirmButtonClass: "btn btn-secondary"
                                });
                            }
                            else {
                                var cek = $.ajax({
                                    contentType: "application/json",
                                    method: "POST",
                                    data: JSON.stringify({ fileName: sfile }),
                                    url: "/Perjanjian/CekFiles"
                                });
                                cek.done(function (data) {
                                    if (data.STS === 'E') {
                                        upload.submit();
                                        swal({
                                            title: "Upload success !",
                                            //text: data.MSG,
                                            type: "success",
                                            buttonsStyling: false,
                                            confirmButtonText: "Oke !",
                                            confirmButtonClass: "btn btn-secondary"
                                        });
                                        $("#inputFile").val('');
                                        $('#inputFileName').val('');
                                        initTableAttachment();
                                    }
                                    else {
                                        swal({
                                            title: "Error!",
                                            text: data.MSG,
                                            type: "error",
                                            buttonsStyling: false,
                                            confirmButtonText: "Oke !",
                                            confirmButtonClass: "btn btn-secondary"
                                        });
                                    }
                                });
                            }
                        });
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "Data perjanjian tidak ditemukan atau belum disimpan.",
                            type: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Oke !",
                            confirmButtonClass: "btn btn-secondary"
                        });
                    }

                });
            }
        });

    });

    $('#btnRefresh').click(function () {
        initTableAttachment();
    });

    $('#tableAttachment').on('click', '#btn-download', function (e) {
        var xTable = $('#tableAttachment').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        var sfile = x["FILENAME"];
        var cek = $.ajax({
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify({ fileName: sfile }),
            url: "/Perjanjian/CekFiles"
        });
        cek.done(function (data) {
            if (data.STS === 'S') {
                swal({
                    title: "Download file ?",
                    text: "<strong>" + sfile + "</strong>  </br></br>  <a href='FileUpload/" + sfile + "' target='_blank'><button class='btn btn-success'>DOWNLOAD FILE</button></a>",
                    html: true,
                    type: "warning",
                    showConfirmButton: false
                });
            } else {
                swal({
                    title: data.MSG,
                    text: "Lampiran tidak ditemukan. Mohon hubungi Admin untuk mengunggah ulang lampiran tersebut.",
                    type: "error",
                    confirmButtonText: "Oke !",
                    confirmButtonClass: "btn-secondary"
                });
            }
        });
    });

    $('#tableAttachment').on('click', '#btn-hapus', function (e) {
        var xTable = $('#tableAttachment').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        var sfile = x["FILENAME"];
        swal({
            title: "Apakah anda yakin ?",
            text: "Lampiran ini akan dihapus permanen. </br></br>" + sfile,
            html: true,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, hapus lampiran!",
            cancelButtonText: "Tidak, batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    var param = {
                        XAKSI: "D",
                        XKODE_PERJANJIAN: x["KODE_PERJANJIAN"],
                        XFILENAME: x["FILENAME"]
                    };
                    var req = $.ajax({
                        contentType: "application/json",
                        method: "POST",
                        data: JSON.stringify(param),
                        url: "/Perjanjian/attachmentIUD"
                    });
                    req.done(function (data) {
                        var del = $.ajax({
                            contentType: "application/json",
                            method: "POST",
                            data: JSON.stringify({ fileName: sfile }),
                            url: "/Perjanjian/DeleteFiles"
                        });
                        del.done(function (data2) {
                            initTableAttachment();
                            swal({
                                title: "Success!",
                                text: data2.MSG,
                                type: "success",
                                confirmButtonText: "Oke !",
                                confirmButtonClass: "btn-success"
                            });
                        });
                        //}
                    });
                } else {
                    swal({
                        title: "Cancelled",
                        text: "Lampiran batal dihapus :)",
                        type: "error",
                        confirmButtonClass: "btn-danger"
                    });
                }
            });

    });



    //$('#inputNoRec').val("1");

    //$('#inputFileName').click(function () {
    //    var RecID = $('#inputNoRec').val();
    //    if (RecID) {
    //        $("#inputFile").trigger('click');
    //    }
    //    else {
    //        swal({
    //            title: "Error!",
    //            text: "Rec ID tidak ditemukan.",
    //            type: "error",
    //            buttonsStyling: false,
    //            confirmButtonText: "Oke !",
    //            confirmButtonClass: "btn btn-label-brand"
    //        });
    //    }
    //});

    //$('#btn-clear').click(function () {
    //    $("#inputFile").val('');
    //    $('#inputFileName').val('');
    //});

    //$("input[type='file']").change(function (e) {
    //    var NoRec = $('#inputNoRec').val();
    //    //var NoDok = $('#inputNomorDok').val();
    //    //var NoDoc = NoDok.replace(/\//g, "_");
    //    $('#inputFileName').val(NoRec + '-' + e.target.files[0].name);
    //    var fSize = e.target.files[0].size / 1024 / 1024;
    //    var fileSize = Math.round(fSize).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //    var maxSize = 50;
    //    if (fileSize >= maxSize) {
    //        swal({
    //            title: "File to large!",
    //            text: "Max size: " + maxSize + " MB.",
    //            type: "warning",
    //            confirmButtonClass: "btn-primary",
    //            confirmButtonText: "Oke !",
    //            closeOnCancel: false
    //        },
    //            function (isConfirm) {
    //                if (isConfirm) {
    //                    $("#inputFile").val('');
    //                    $('#inputFileName').val('');
    //                }
    //            });
    //    }
    //});

    //$('#inputFile').fileupload({
    //    dataType: 'json',
    //    url: '/Perjanjian/UploadFiles',
    //    add: function (e, data) {
    //        var NoRec = $('#inputNoRec').val();
    //        var count = data.files.length;
    //        var i;
    //        for (i = 0; i < count; i++) {
    //            data.files[i].uploadName = NoRec + '-' + data.files[i].name;
    //            var sfile = data.files[i].uploadName;
    //            var upload = data;
    //        }
    //        $('#btnUpload').click(function () {
    //            var Rec_ID = $('#inputNoRec').val();
    //            if (Rec_ID) {
    //                var param = {
    //                    XAKSI: "I",
    //                    XKODE_PERJANJIAN: $('#inputNoRec').val(),
    //                    XFILENAME: sfile
    //                };
    //                var req = $.ajax({
    //                    contentType: "application/json",
    //                    method: "POST",
    //                    data: JSON.stringify(param),
    //                    url: "/Perjanjian/attachmentIUD"
    //                });
    //                req.done(function (data) {
    //                    if (data.STS === 'E') {
    //                        swal({
    //                            title: "Error!",
    //                            text: data.MSG,
    //                            type: "error",
    //                            buttonsStyling: false,
    //                            confirmButtonText: "Oke !",
    //                            confirmButtonClass: "btn btn-secondary"
    //                        });
    //                    }
    //                    else {
    //                        var cek = $.ajax({
    //                            contentType: "application/json",
    //                            method: "POST",
    //                            data: JSON.stringify({ fileName: sfile }),
    //                            url: "/Perjanjian/CekFiles"
    //                        });
    //                        cek.done(function (data) {
    //                            if (data.STS === 'E') {
    //                                upload.submit();
    //                                swal({
    //                                    title: "Upload success !",
    //                                    //text: data.MSG,
    //                                    type: "success",
    //                                    buttonsStyling: false,
    //                                    confirmButtonText: "Oke !",
    //                                    confirmButtonClass: "btn btn-secondary"
    //                                });
    //                                $("#inputFile").val('');
    //                                $('#inputFileName').val('');
    //                                initTableAttachment();
    //                            }
    //                            else {
    //                                swal({
    //                                    title: "Error!",
    //                                    text: data.MSG,
    //                                    type: "error",
    //                                    buttonsStyling: false,
    //                                    confirmButtonText: "Oke !",
    //                                    confirmButtonClass: "btn btn-secondary"
    //                                });
    //                            }
    //                        });

    //                    }

    //                    //var cek2 = $.ajax({
    //                    //    contentType: "application/json",
    //                    //    method: "POST",
    //                    //    data: JSON.stringify({ fileName: sfile }),
    //                    //    url: "/Perjanjian/CekFiles"
    //                    //});
    //                    //cek2.done(function (data2) {
    //                    //    if (data2.STATUS === 'S') {
    //                    //        swal({
    //                    //            title: "Upload success !",
    //                    //            //text: data.MSG,
    //                    //            type: "success",
    //                    //            buttonsStyling: false,
    //                    //            confirmButtonText: "Oke !",
    //                    //            confirmButtonClass: "btn btn-secondary"
    //                    //        });
    //                    //        $("#inputFile").val('');
    //                    //        $('#inputFileName').val('');
    //                    //    }
    //                    //    else {
    //                    //        var paramD = {
    //                    //            XAKSI: "D",
    //                    //            XKODE_PERJANJIAN: $('#inputNoRec').val(),
    //                    //            XFILENAME: sfile
    //                    //        };
    //                    //        var del = $.ajax({
    //                    //            contentType: "application/json",
    //                    //            method: "POST",
    //                    //            data: JSON.stringify(paramD),
    //                    //            url: "/Perjanjian/attachmentIUD"
    //                    //        });
    //                    //        del.done(function (data3) {
    //                    //            if (data3 === 'E') {
    //                    //                swal({
    //                    //                    title: "Error!",
    //                    //                    text: data3.MSG,
    //                    //                    type: "error",
    //                    //                    buttonsStyling: false,
    //                    //                    confirmButtonText: "Oke !",
    //                    //                    confirmButtonClass: "btn btn-secondary"
    //                    //                });
    //                    //            }
    //                    //            else {
    //                    //                swal({
    //                    //                    title: "Gagal menyimpan lampiran.",
    //                    //                    //text: data3.MSG,
    //                    //                    type: "error",
    //                    //                    buttonsStyling: false,
    //                    //                    confirmButtonText: "Oke !",
    //                    //                    confirmButtonClass: "btn btn-secondary"
    //                    //                });
    //                    //            }
    //                    //        });
    //                    //    }
    //                    //});
    //                });
    //            }
    //            else {
    //                swal({
    //                    title: "Error!",
    //                    text: "Data perjanjian tidak ditemukan atau belum disimpan.",
    //                    type: "error",
    //                    buttonsStyling: false,
    //                    confirmButtonText: "Oke !",
    //                    confirmButtonClass: "btn btn-secondary"
    //                });
    //            }
                
    //        });
    //    }
    //});

    //$('#inputFile').fileupload({
    //    dataType: 'json',
    //    url: '/Perjanjian/UploadFiles',
    //    add: function (e, data) {
    //        var NoRec = $('#inputNoRec').val();
    //        var NoDok = $('#inputNomorDok').val();
    //        var NoDoc = NoDok.replace(/\//g, "_");
    //        var count = data.files.length;
    //        var i;
    //        for (i = 0; i < count; i++) {
    //            data.files[i].uploadName = NoRec + '-' + data.files[i].name;
    //        }
    //        $('#btnUpload').click(function () {
    //            data.submit();
    //            simpanUpload(); 
    //            hapusAttachment();
    //        });
    //    },
    //    done: function (e, data) {
    //        $('#labelMSG').text(data.result.name);
    //    }
    //});

    //var simpanUpload = function () {
    //    var xAksi = "I";
    //    var KodePerjanjian = $('#inputNoRec').val();
    //    var Filename = $('#inputFileName').val();
    //    if (KodePerjanjian && Filename) {
    //        var param = {
    //            XAKSI: xAksi,
    //            XKODE_PERJANJIAN: KodePerjanjian,
    //            XFILENAME: Filename
    //        };
    //        var req = $.ajax({
    //            contentType: "application/json",
    //            data: JSON.stringify(param),
    //            method: "POST",
    //            url: "/Perjanjian/attachmentIUD",
    //            timeout: 30000
    //        });
    //        req.done(function (data) {
    //            console.log('simpanUpload : ' + data.MSG);
    //            initTableAttachment();
    //            hapusAttachment();
    //            $("#inputFile").val('');
    //            $('#inputFileName').val(''); 
    //        });
    //    } else {
    //        swal({
    //            title: "Peringatan !",
    //            text: "Anda belum memilih file atau mengisi data dengan lengkap.",
    //            type: "warning",
    //            confirmButtonClass: "btn-danger",
    //            confirmButtonText: "Cancel"
    //        });
    //    }
    //}


    //var btnHapus = function () {
    //    $('#tableAttachment').on('click', '#btn-hapus', function (e) {
    //        var jsonData = JSON.stringify({ sFile: fName });
    //        console.log('Deleting ' + jsonData);
    //        deleteFile(jsonData);
    //    });
    //}

    

    //var hapusAttachment = function () {
    //    $('#tableAttachment').on('click', '#btn-hapus', function (e) {
    //        var xTable = $('#tableAttachment').dataTable();
    //        var nRow = $(this).parents('tr')[0];
    //        var x = xTable.fnGetData(nRow);
    //        var dataD = {
    //            XAKSI: "D",
    //            XKODE_PERJANJIAN: x["KODE_PERJANJIAN"],
    //            XFILENAME: x["FILENAME"]
    //        }
    //        var req = $.ajax({
    //            contentType: "application/json",
    //            method: "POST",
    //            data: JSON.stringify(dataD),
    //            url: "/Perjanjian/attachmentIUD"
    //        });
    //        req.done(function (data) {
    //            console.log('hapusAttachment :' + data.MSG);
    //            initTableAttachment();
    //            deleteFile(x["FILENAME"]);
    //        });
    //    });
    //}

    //var deleteFile = function (sFile) {
    //    $.ajax({
    //        type: "POST",
    //        url: "/Perjanjian/DeleteFiles",
    //        data: sFile,
    //        contentType: "application/json; charset-utf8",
    //        dataType: "jsondata",
    //        async: "true",
    //        success: function (response) {
    //            console.log("deleteFile: success" + response);
    //        },
    //        error: function (response) {
    //            console.log(response);
    //        }
    //    });
    //}
    // END Attachment





    $('#btnLihatData').click(function () {
        initTablePerjanjian();
        $('#modalData').modal({ backdrop: 'static', keyboard: false });
    });

    var btnSimpan = function () {
        $('#btn-simpan').click(function () {
            simpanHeader();
        });
    }

    var simpanHeader = function () {
        var NoRec = $('#inputNoRec').val();
        var NoDok = $('#inputNomorDok').val();
        var Judul = $('#inputJudul').val();
        var Tgl1 = $('#inputTglBerlaku').val();
        var Tgl2 = $('#inputTglBerakhir').val();
        var TglPerjanjian = $('#inputTglPerjanjian').val();
        var Deskripsi = $('#inputDeskripsi').val();
        var JnsPerjanjian = $('#inputJnsPerjanjian').val();
        var User = $('#UserId').val();
        if (NoDok && Judul && Deskripsi && Tgl1 && Tgl2 && JnsPerjanjian) {
            if (NoRec > 0) {
                xAksi = "U";
            } else {
                xAksi = "I";
            }
            var param = {
                XAKSI: xAksi,
                XRECID: NoRec,
                XNO_DOK: NoDok,
                XJUDUL: Judul,
                XDESKRIPSI: Deskripsi,
                XTGL1: Tgl1,
                XTGL2: Tgl2,
                XTGLPERJANJIAN: TglPerjanjian,
                XJNS_PERJANJIAN: JnsPerjanjian,
                XUSER: User
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Perjanjian/perjanjianIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    swal({
                        title: "Error !",
                        text: data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                } else {
                    swal({
                        title: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke",
                        closeOnConfirm: true,
                        closeOnCancel: false
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                clearform();
                            }
                        });
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Anda belum mengisikan data dengan lengkap.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    var simpanHeadersilent = function () {
        //var xAksi = "I";
        var NoRec = $('#inputNoRec').val();
        var NoDok = $('#inputNomorDok').val();
        var Judul = $('#inputJudul').val();
        var Deskripsi = $('#inputDeskripsi').val();
        var Tgl1 = $('#inputTglBerlaku').val();
        var Tgl2 = $('#inputTglBerakhir').val();
        var TglPerjanjian = $('#inputTglPerjanjian').val();
        var JnsPerjanjian = $('#inputJnsPerjanjian').val();
        var User = $('#UserId').val();
        if (NoDok && Judul && Deskripsi && Tgl1 && Tgl2 && JnsPerjanjian) {
            if (NoRec > 0) {
                xAksi = "U";
            } else {
                xAksi = "I";
            }
            var param = {
                XAKSI: xAksi,
                XRECID: NoRec,
                XNO_DOK: NoDok,
                XJUDUL: Judul,
                XDESKRIPSI: Deskripsi,
                XTGL1: Tgl1,
                XTGL2: Tgl2,
                XTGLPERJANJIAN: TglPerjanjian,
                XJNS_PERJANJIAN: JnsPerjanjian,
                XUSER: User
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Perjanjian/perjanjianIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    //swal({
                    //    title: "Error !",
                    //    text: data.MSG,
                    //    type: "error",
                    //    confirmButtonClass: "btn-danger",
                    //    confirmButtonText: "Cancel"
                    //});
                    console.log(param);
                } else {
                    console.log('simpanHeadersilent : ' + data.MSG);
                    $('#inputNoRec').val('' + data.STS + '');
                    $("#divPlg").show();
                    $("#inputPelanggan").val('');
                    select2pelanggan();
                    initTablePelanggan();
                    //simpanPelanggan();
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Anda belum mengisikan data dengan lengkap.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    var initTablePerjanjian = function () {
        $('#tablePerjanjian').DataTable({
            "ajax": {
                "url": "/Perjanjian/getDatatablePerjanjian",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFind').val();
                    //data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<span>' + full["NO_DOKUMEN"] + '</span></br>' +
                            '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<span><strong>' + full["NM_PERJANJIAN"] +'</strong></span></br>' +
                            '<span>' + full["JUDUL"] + '</span>';
                    }
                },
                //{
                //    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                //    "render": function (data, type, full) {
                //        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                //            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                //    }
                //},
                {
                    "data": "STATUS", "name": "STATUS",
                    "render": function (data, type, full) {
                        return '<span>' + full["STATUS"] + '</span></br>' +
                            '<small class="text-muted">Updated date: ' + full["STATUS_DATE"] + '</small>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full, meta) {
                        //var attch = '<span class="badge badge-pill badge-' + ((data === 'A') ? "success" : "danger") + '">' + ((data === 'A') ? "AKTIF" : "TIDAK AKTIF") + '</span>';
                        var attch = '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                        return attch;
                    }
                },
                //{
                //    "data": "CREATED_BY", "name": "CREATED_BY",
                //    "render": function (data, type, full) {
                //        var ret = '<span><strong>Created by: </strong>' + full["CREATED_BY"] + '</span><small class="text-muted"> ' + full["CREATED_DATE"] + '</small></br>' +
                //            '<span><strong>Updated by: </strong>' + full["UPDATED_BY"] + '</span><small class="text-muted"> ' + full["UPDATED_DATE"] + '</small>';
                //        return ret;
                //    }
                //},
                {
                    "render": function (data, type, full) {
                        //var aksi = '<div class="pull-right">' +
                        //    '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                        //    '<a class="btn btn-sm btn-link" id="btn-lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Data</a>' +
                        //    '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                        //    '</div>' +
                        //    '</div>';
                        var aksi = '<div class="dropdown">' +
                            '<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
                            'Actions' +
                            '</button>' +
                            '<div class="dropdown-menu" role="menu">' +
                            '<a class="dropdown-item" id="btn-lihat"><i class="dropdown-icon icmn-search" aria-hidden="true"></i> Lihat Data</a>' +
                            '<div class="dropdown-divider"></div>' +
                            '<a class="dropdown-item" id="btn-hapus"><i class="dropdown-icon icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';                        
                        return aksi;
                    }
                }
            ],
            "responsive": true,
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            //"paging": false,
            "iDisplayLength": 3,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "15%", "targets": 1 }
            ]
        });

        var table = $('#tablePerjanjian').DataTable();
        $('#txtFind').keypress(function () {
            if (event.keyCode === 13) {
                $("#tablePerjanjian_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }

    var hapusperjanjian = function () {
        $('#tablePerjanjian').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tablePerjanjian').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            
            swal({
                title: "Apakah anda yakin akan menghapus perjanjian ini ?",
                text: "Judul: "+ x["JUDUL"],
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Hapus",
                cancelButtonText: "Batal",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        var xAksi = "D";
                        var dataD = {
                            XAKSI: xAksi,
                            XRECID: x["REC_ID"],
                            XNO_DOK: "",
                            XJUDUL: "",
                            XDESKRIPSI: "",
                            XTGL1: "",
                            XTGL2: "",
                            XTGLPERJANJIAN: "",
                            XJNS_PERJANJIAN: "",
                            XUSER: ""
                        }
                        var req = $.ajax({
                            contentType: "application/json",
                            method: "POST",
                            data: JSON.stringify(dataD),
                            url: "/Perjanjian/perjanjianIUD"
                        });
                        req.done(function (data) {
                            if (data.STS === 'S') {
                                initTablePerjanjian();
                                console.log(data.MSG);
                                swal({
                                    title: "Deleted!",
                                    text: data.MSG,
                                    type: "success",
                                    confirmButtonClass: "btn-success"
                                });
                            } else {
                                console.log(data.MSG);
                                swal({
                                    title: "Error!",
                                    text: data.MSG,
                                    type: "warning",
                                    confirmButtonClass: "btn-danger"
                                });
                            }
                        })
                            .fail(function () {
                                console.log(data.MSG);
                                swal({
                                    title: "Error!",
                                    text: data.MSG,
                                    type: "error",
                                    confirmButtonClass: "btn-danger"
                                });
                            });
                    } else {
                        swal({
                            title: "Cancelled",
                            text: "Data batal dihapus.",
                            type: "error",
                            confirmButtonClass: "btn-danger"
                        });
                    }
                });
            
        });
    }

    var lihatperjanjian = function () {
        $('#tablePerjanjian').on('click', '#btn-lihat', function (e) {
            var xTable = $('#tablePerjanjian').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            $('#inputNoRec').val(x["REC_ID"]);
            $('#inputJnsPerjanjian').append("<option value='" + x["JNS_PERJANJIAN"] + "' selected>" + x["NM_PERJANJIAN"] + "</option>");
            //$('#inputJnsPerjanjian').text(x["NM_PERJANJIAN"]);
            $('#inputNomorDok').val(x["NO_DOKUMEN"]);
            $('#inputTglBerlaku').val(x["TGL_BERLAKU"]);
            $('#inputTglBerakhir').val(x["TGL_BERAKHIR"]);
            $('#inputTglPerjanjian').val(x["TGL_PERJANJIAN"]);
            $('#inputJudul').val(x["JUDUL"]);
            $('#inputDeskripsi').val(x["DESKRIPSI"]);

            $('#created_by').text(x["CREATED_BY"]);
            $('#created_date').text(x["CREATED_DATE"]);
            $('#updated_by').text(x["UPDATED_BY"]);
            $('#updated_date').text(x["UPDATED_DATE"]);

            initTablePelanggan();
            initTablePIC();
            initTableUker();
            initTableAttachment();
            initTableAlur();
            $('#modalData').modal('hide');
            btnSimpan();
        });
    }


    // Alur Dokumen
    $('#divAlur').hide();

    var select2alur = function () {
        $("#inputAlur").select2({
            placeholder:"Pilih alur dokumen",
            ajax: {
                url: "/Perjanjian/getSelectAlur",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.NAMA,
                                id: item.KODE,
                                r_text: item.NAMA
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 0
            //minimumInputLength: 0 // untuk menampilkan semua
        });
    }

    var initTableAlur = function () {
        $('#tableAlur').DataTable({
            "ajax": {
                "url": "/Perjanjian/getStat",
                "type": "GET",
                "data": function (data) {
                    //data.search = $('#txtFind').val();
                    data.XKODE_PERJANJIAN = $('#inputNoRec').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NAMA_ALUR", "name": "NAMA_ALUR",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_ALUR"] + '</span>';
                    }
                },
                {
                    "data": "REC_STAT", "name": "REC_STAT",
                    "render": function (data, type, full) {
                        return '<span class="badge badge-pill badge-' + ((data === '1') ? "success" : "info") + '">' + ((data === '1') ? "Selesai" : "Dalam Proses") + '</span><br/>' +
                            '<small class="text-muted">Updated: ' + full["CREATED_DATE"] + '</small><div class="pull-right">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = //'<small class="text-muted">Updated: ' + full["CREATED_DATE"] + '</small><div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-check" download><i class="icmn-checkmark" aria-hidden="true"></i> Selesai</a>' +
                            '<a class="btn btn-sm btn-link" id="btn-hapus"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        //var aksi = '<a class="btn btn-sm btn-link" id="btn-download"><i class="icmn-download" aria-hidden="true"></i> Download</a>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": ""
            },
            "filter": false
        });
    }

    $('#btnTambahAlur').click(function () {
        $('#divAlur').show();
        simpanAlur();
    });

    var simpanAlur = function () {
        var xAksi = "I";
        var KodePerjanjian = $('#inputNoRec').val();
        var Alur = $('#inputAlur').val();
        if (KodePerjanjian && Alur) {
            var param = {
                XAKSI: xAksi,
                XKODE_PERJANJIAN: KodePerjanjian,
                XKODE_ALUR: Alur
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Perjanjian/alurIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                } else {
                    console.log(data.MSG);
                    $('#inputAlur').val('');
                    select2alur();
                    initTableAlur();
                }
            });
        } //else {
        //    swal({
        //        title: "Peringatan !",
        //        text: "Anda belum memilih status dokumen.",
        //        type: "warning",
        //        confirmButtonClass: "btn-danger",
        //        confirmButtonText: "Cancel"
        //    });
        //}
    }

    var hapusAlur = function () {
        $('#tableAlur').on('click', '#btn-hapus', function (e) {
            var xTable = $('#tableAlur').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var dataD = {
                XAKSI: "D",
                //XID: x["ID"],
                XKODE_PERJANJIAN: x["KODE_PERJANJIAN"],
                XKODE_ALUR: x["KODE_ALUR"]
            }
            var req = $.ajax({
                contentType: "application/json",
                method: "POST",
                data: JSON.stringify(dataD),
                url: "/Perjanjian/alurIUD"
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    console.log(data.MSG);
                    initTableAlur();
                } else {
                    console.log(data.MSG);
                }
            })
                .fail(function () {
                    console.log(data.MSG);
                });
        });
    }

    var cekAlur = function () {
        $('#tableAlur').on('click', '#btn-check', function (e) {
            var xTable = $('#tableAlur').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            var dataD = {
                XAKSI: "U",
                //XID: x["ID"],
                XKODE_PERJANJIAN: x["KODE_PERJANJIAN"],
                XKODE_ALUR: x["KODE_ALUR"]
            }
            var req = $.ajax({
                contentType: "application/json",
                method: "POST",
                data: JSON.stringify(dataD),
                url: "/Perjanjian/alurIUD"
            });
            req.done(function (data) {
                if (data.STS === 'S') {
                    console.log(data.MSG);
                    initTableAlur();
                } else {
                    console.log(data.MSG);
                }
            })
                .fail(function () {
                    console.log(data.MSG);
                });
        });
    }

    var clearform = function () {
        $('#inputNoRec').val('');
        $('#inputJnsPerjanjian').val('');
        $("#inputJnsPerjanjian").select2({
            placeholder: 'Pilih jenis perjanjian',
            ajax: {
                url: "/Perjanjian/getJnsPerjanjian",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        p: params.term,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.NAMA,// + ' (' + item.KODE + ')',
                                id: item.KODE,
                                r_text: item.NAMA
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 0
            //minimumInputLength: 0 // untuk menampilkan semua
        });
        
        $('#inputNomorDok').val('');
        $('#inputTglBerlaku').val('');
        $('#inputTglBerakhir').val('');
        $('#inputTglPerjanjian').val('');
        $('#inputJudul').val('');
        $('#inputDeskripsi').val('');

        $('#labelFileName').val('');
        $("#inputFile").val('');
        $('#inputFileName').val('');

        $('#created_by').text('');
        $('#created_date').text('');
        $('#updated_by').text('');
        $('#updated_date').text('');

        initTablePelanggan();
        initTablePIC();
        initTableUker();
        initTableAttachment();
        initTableAlur();

        $('#divAlur').hide();
        $("#divPlg").hide();
        $("#divPIC").hide();
        $("#divUker").hide();
    }

    var bersihkan = function () {
        $('#btn-bersihkan').click(function () {
            window.location.reload();
            //clearform();
            //initTablePerjanjian();
        });
    }

    return {
        init: function () {
            //dropify();
            bersihkan();
            initTablePerjanjian();
            lihatperjanjian();
            hapusperjanjian();
            btnSimpan();
            initTablePelanggan();
            initTablePIC();
            initTableUker();
            btnAddpelanggan();
            hapusPelanggan();
            btnAddPIC();
            hapusPIC();
            initTableAttachment();
            //hapusAttachment();
            select2alur();
            initTableAlur();
            hapusAlur();
            cekAlur();
            buttonArsip();
        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});