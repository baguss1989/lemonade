﻿var Index = function () {

    $('#btn-lihatdata').click(function () {
        initMasterPelanggan();
        $('#modalData').modal({ backdrop: 'static', keyboard: false });
        //hapusUser();
    });

    var initMasterPelanggan = function () {
        $('#tablePelanggan').DataTable({
            "ajax": {
                "url": "/Pelanggan/getmasterPelanggan",
                "type": "GET",
                "data": function (data) {
                    
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NAMA_PELANGGAN", "name": "NAMA_PELANGGAN",
                    "render": function (data, type, full, meta) {
                        var ret = '<span>' + full["NAMA_PELANGGAN"] + '</span></br>' +
                            '<span class="text-muted">(' + full["KODE_PELANGGAN"] + ')</span>';
                        return ret;
                    }
                },
                {
                    "data": "ALAMAT"
                },
                {
                    "data": "EMAIL"
                },
                {
                    "data": "REC_STAT", "name": "REC_STAT",
                    "render": function (data, type, full, meta) {
                        var sts = '<span class="badge badge-pill badge-' + ((data === 'A') ? "success" : "danger") + '">' + ((data === 'A') ? "AKTIF" : "TIDAK AKTIF") + '</span>';
                        return sts;
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="dropdown">' +
                            '<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
                            'Actions' +
                            '</button>' +
                            '<div class="dropdown-menu" role="menu">' +
                            '<a class="dropdown-item" id="btn-edit"><i class="dropdown-icon icmn-pencil" aria-hidden="true"></i> Edit</a>' +
                            '<div class="dropdown-divider"></div>' +
                            '<a class="dropdown-item" id="btn-hapus"><i class="dropdown-icon icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            //"serverSide": true,
            "processing": true,
            "destroy": true
        });
    }

    var btnsimpan = function () {
        $('#btn-simpan').click(function () {
            simpan();
            initMasterPelanggan();
        });
    }

    var simpan = function () {
        var KodePlg = $('#inputKodePlg').val();
        var NamaPlg = $('#inputNamaPlg').val();
        if (NamaPlg) {
            if (KodePlg) {
                xAksi = "U";
            } else {
                xAksi = "I";
            }
            var param = {
                XAKSI: xAksi,
                XKODE_PELANGGAN: KodePlg,
                XNAMA_PELANGGAN: NamaPlg,
                XALAMAT: $('#inputAlamatPlg').val(),
                XKOTA: $('#inputKota').val(),
                XTLP1: $('#inputTlp1').val(),
                XTLP2: $('#inputTlp2').val(),
                XEMAIL: $('#inputEmail').val(),
                XREC_STAT: $('#inputStatus').val(),
                XUSER: $('#UserId').val()
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/Pelanggan/MasterPelangganIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                    swal({
                        title: "Error !",
                        text: "Pesan error : " + data.MSG,
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                    
                } else {
                    console.log(data.MSG);
                    $('#inputKodePlg').val(data.STS);
                    swal({
                        title: "Berhasil !",
                        text: data.MSG,
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Oke"
                    });
                    initMasterPelanggan();
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Nama Pelanggan harus diisi.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    $('#btn-bersihkan').click(function () {
        resetform();
    });

    var resetform = function () {
        $('#inputKodePlg').val('');
        $('#inputNamaPlg').val('');
        $('#inputAlamatPlg').val('');
        $('#inputKota').val('');
        $('#inputTlp1').val('');
        $('#inputTlp2').val('');
        $('#inputEmail').val('');
        $('#inputStatus').val('');
    }

    $('#tablePelanggan').on('click', '#btn-edit', function (e) {
        var xTable = $('#tablePelanggan').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        $('#inputKodePlg').val(x["KODE_PELANGGAN"]);
        $('#inputNamaPlg').val(x["NAMA_PELANGGAN"]);
        $('#inputAlamatPlg').val(x["ALAMAT"]);
        $('#inputKota').val(x["KOTA"]);
        $('#inputTlp1').val(x["TLP1"]);
        $('#inputTlp2').val(x["TLP2"]);
        $('#inputEmail').val(x["EMAIL"]);
        $('#inputStatus').val(x["REC_STAT"]);
        $('#modalData').modal('hide');
    });

    $('#tablePelanggan').on('click', '#btn-hapus', function (e) {
        var xTable = $('#tablePelanggan').dataTable();
        var nRow = $(this).parents('tr')[0];
        var x = xTable.fnGetData(nRow);
        swal({
            title: "Konfirmasi !",
            text: "Anda akan menghapus data  " + x["NAMA_PELANGGAN"] + " (" + x["KODE_PELANGGAN"]+")",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, remove it",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    var dataD = {
                        XAKSI: "D",
                        XKODE_PELANGGAN: x["KODE_PELANGGAN"],
                        XNAMA_PELANGGAN: "",
                        XALAMAT: "",
                        XKOTA: "",
                        XTLP1: "",
                        XTLP2: "",
                        XEMAIL: "",
                        XREC_STAT: "",
                        XUSER: ""
                    }
                    var req = $.ajax({
                        contentType: "application/json",
                        method: "POST",
                        data: JSON.stringify(dataD),
                        url: "/Pelanggan/MasterPelangganIUD"
                    });
                    req.done(function (data) {
                        if (data.STS === 'S') {
                            swal({
                                title: data.MSG,
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Oke"
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        initMasterPelanggan();
                                        resetform();
                                    }
                                });
                            console.log(data.MSG);
                        } else {
                            swal({
                                title: data.MSG,
                                type: "error",
                                confirmButtonClass: "btn-danger"
                            });
                            console.log(data.MSG);
                        }
                    })
                        .fail(function () {
                            swal({
                                title: data.MSG,
                                type: "error",
                                confirmButtonClass: "btn-danger"
                            });
                            console.log(data.MSG);
                        });
                } else {
                    swal({
                        title: "Cancelled",
                        text: "Pelanggan " + x["NAMA_PELANGGAN"] + "batal dihapus.",
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ya"
                    });
                }
            });
    });
    
    return {
        init: function () {
            initMasterPelanggan();
            btnsimpan();
        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});