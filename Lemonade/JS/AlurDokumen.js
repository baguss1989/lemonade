﻿var Index = function () {

    $('#btn-update').hide();

    $('#btn-simpan').click(function () {
        simpan();
    });

    $('#btn-delete').click(function () {
        hapus();
    });

    $('#btn-update').click(function () {
        edit();
    });

    $('#btn-batal').click(function () {
        $('#btn-update').hide();
        $('#btn-simpan').show();
        $('#inputAlur').val('');
        $('#inputRecId').val('');
    });

    var initTableAlur = function () {
        $('#tableAlurDokumen').DataTable({
            "ajax": {
                "url": "/AlurDokumen/getAlurDokumen",
                "type": "GET",
                "data": function (data) {
                    
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NAMA_ALUR", "name": "NAMA_ALUR",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_ALUR"] + '</span>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-edit" download><i class="icmn-pencil" aria-hidden="true"></i> Edit</a>' +
                            '<a class="btn btn-sm btn-link" id="btn-delete"><i class="icmn-bin" aria-hidden="true"></i> Hapus</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            //"searching": true,
            //"bLengthChange": true,
            "paging": false,
            //"iDisplayLength": 20,
            //"autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false
        });
    }

    var simpan = function () {
        var xAksi = "I";
        var NamaAlur = $('#inputAlur').val();
        if (NamaAlur) {
            var param = {
                XAKSI: xAksi,
                XNAMA_ALUR: NamaAlur
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/AlurDokumen/alurdokumenIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                    swal({
                        title: "Error !",
                        text: "Error Message: " + data.MSG,
                        type: "danger",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                } else {
                    console.log(data.MSG);
                    initTableAlur();
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Data harus diisi.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    var edit = function () {
        var xAksi = "U";
        var RecId = $('#inputRecId').val();
        var NamaAlur = $('#inputAlur').val();
        if (NamaAlur) {
            var param = {
                XAKSI: xAksi,
                XREC_ID: RecId,
                XNAMA_ALUR: NamaAlur
            };
            var req = $.ajax({
                contentType: "application/json",
                data: JSON.stringify(param),
                method: "POST",
                url: "/AlurDokumen/alurdokumenIUD",
                timeout: 30000
            });
            req.done(function (data) {
                if (data.STS === 'E') {
                    console.log(data.MSG);
                    swal({
                        title: "Error !",
                        text: "Error Message: " + data.MSG,
                        type: "danger",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Cancel"
                    });
                } else {
                    console.log(data.MSG);
                    initTableAlur();
                    $('#btn-update').hide();
                    $('#btn-simpan').show();
                    $('#inputAlur').val('');
                    $('#inputRecId').val('');
                }
            });
        } else {
            swal({
                title: "Peringatan !",
                text: "Data harus diisi.",
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Cancel"
            });
        }
    }

    var hapus = function () {
        $('#tableAlurDokumen').on('click', '#btn-delete', function (e) {
            var xTable = $('#tableAlurDokumen').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            swal({
                title: "Konfirmasi !",
                text: "Anda akan menghapus data ini.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, remove it",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        var dataD = {
                            XAKSI: "D",
                            XREC_ID: x["REC_ID"],
                            XNAMA_ALUR: x["NAMA_ALUR"]
                        }
                        var req = $.ajax({
                            contentType: "application/json",
                            method: "POST",
                            data: JSON.stringify(dataD),
                            url: "/AlurDokumen/alurdokumenIUD"
                        });
                        req.done(function (data) {
                            if (data.STS === 'S') {
                                swal({
                                    title: "Deleted!",
                                    text: "Berhasil. " + data.MSG,
                                    type: "success",
                                    confirmButtonClass: "btn-success"
                                });
                                console.log(data.MSG);
                                initTableAlur();
                            } else {
                                swal({
                                    title: "Error!",
                                    text: "Error Message: " + data.MSG,
                                    type: "error",
                                    confirmButtonClass: "btn-danger"
                                });
                                console.log(data.MSG);
                            }
                        })
                            .fail(function () {
                                console.log(data.MSG);
                            });
                    } else {
                        swal({
                            title: "Cancelled",
                            text: "Data batal dihapus.",
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ya"
                        });
                    }
                });
        });
    }

    var lookup = function () {
        $('#tableAlurDokumen').on('click', '#btn-edit', function (e) {
            var xTable = $('#tableAlurDokumen').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);
            $('#inputRecId').val(x["REC_ID"]);
            $('#inputAlur').val(x["NAMA_ALUR"]);
            $('#btn-update').show();
            $('#btn-simpan').hide();
        });
    }

    return {
        init: function () {
            initTableAlur();
            hapus();
            lookup();
        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});