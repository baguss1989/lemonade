﻿var Index = function () {

    // Chartistcard

    //new Chartist.Line(".chart-area-1", {
    //    series: [
    //        [1,0,3,0,0,0,0]
    //        //[2, 11, 8, 14, 18, 20, 15]
    //    ]
    //}, {
    //        width: "120px",
    //        height: "107px",

    //        showPoint: false,
    //        showLine: true,
    //        showArea: true,
    //        fullWidth: true,
    //        showLabel: false,
    //        axisX: {
    //            showGrid: false,
    //            showLabel: false,
    //            offset: 0
    //        },
    //        axisY: {
    //            showGrid: false,
    //            showLabel: false,
    //            offset: 0
    //        },
    //        chartPadding: 0,
    //        low: 0,
    //        plugins: [
    //            Chartist.plugins.tooltip()
    //        ]
    //    });

    //new Chartist.Line(".chart-area-2", {
    //    series: [
    //        [20, 80, 67, 120, 132, 66, 97]
    //    ]
    //}, {
    //        width: "120px",
    //        height: "107px",

    //        showPoint: false,
    //        showLine: true,
    //        showArea: true,
    //        fullWidth: true,
    //        showLabel: false,
    //        axisX: {
    //            showGrid: false,
    //            showLabel: false,
    //            offset: 0
    //        },
    //        axisY: {
    //            showGrid: false,
    //            showLabel: false,
    //            offset: 0
    //        },
    //        chartPadding: 0,
    //        low: 0,
    //    });

    //new Chartist.Line(".chart-area-3", {
    //    series: [
    //        [42, 40, 80, 67, 84, 20, 97]
    //    ]
    //}, {
    //        width: "120px",
    //        height: "107px",

    //        showPoint: false,
    //        showLine: true,
    //        showArea: true,
    //        fullWidth: true,
    //        showLabel: false,
    //        axisX: {
    //            showGrid: false,
    //            showLabel: false,
    //            offset: 0
    //        },
    //        axisY: {
    //            showGrid: false,
    //            showLabel: false,
    //            offset: 0
    //        },
    //        chartPadding: 0,
    //        low: 0,
    //    });







    $("[data-toggle=popover-hover]").popover({
        trigger: 'hover'
    });


    $('#generalSearch').keypress(function () {
        if (event.keyCode === 13) {
            $('#txtFind').val($('#generalSearch').val());
            inittableSearch();
            $('#modalTitle').text('Search');
            $('#modalSearch').modal({ backdrop: 'static', keyboard: false });
        }
    });

    var inittableSearch = function () {
        $('#tableSearch').DataTable({
            "ajax": {
                "url": "/Perjanjian/getDatatablePerjanjian",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFind').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 5,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearch').DataTable();
        $('#txtFind').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearch_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }




    var getDraft = function () {
        var req = null;
        req = $.ajax({
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify(),
            method: "POST",
            url: "/Home/getCounterDraft",
            timeout: 30000
        });
        req.done(function (data) {
            $("#badgeDraft").text(data.MSG);
        });
    }

    $('#badgeDraft, #divDraft').click(function () {
        inittableDraft();
        $('#modalTitle').text('Draft');
        $('#modalSearch').modal({ backdrop: 'static', keyboard: false });
    });

    var inittableDraft = function () {
        $('#tableSearch').DataTable({
            "ajax": {
                "url": "/Home/getTableDraft",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFind').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 5,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearch').DataTable();
        $('#txtFind').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearch_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }



    var getBerakhir = function () {
        var req = null;
        req = $.ajax({
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify(),
            method: "POST",
            url: "/Home/getCounterBerakhir",
            timeout: 30000
        });
        req.done(function (data) {
            $("#badgeBerakhir").text(data.MSG);
        });
    }

    $('#badgeBerakhir, #divBerakhir').click(function () {
        inittableBerakhir();
        $('#modalTitle').text('Document Expired');
        $('#modalSearch').modal({ backdrop: 'static', keyboard: false });
    });

    var inittableBerakhir = function () {
        $('#tableSearch').DataTable({
            "ajax": {
                "url": "/Home/getTableBerakhir",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFind').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 5,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearch').DataTable();
        $('#txtFind').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearch_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }




    var getBerlaku = function () {
        var req = null;
        req = $.ajax({
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify(),
            method: "POST",
            url: "/Home/getCounterBerlaku",
            timeout: 30000
        });
        req.done(function (data) {
            $("#badgeBerlaku").text(data.MSG);
        });
    }

    $('#badgeBerlaku, #divBerlaku').click(function () {
        inittableBerlaku();
        $('#modalTitle').text('Document Active');
        $('#modalSearch').modal({ backdrop: 'static', keyboard: false });
    });

    var inittableBerlaku = function () {
        $('#tableSearch').DataTable({
            "ajax": {
                "url": "/Home/getTableBerlaku",
                "type": "GET",
                "data": function (data) {
                    data.search = $('#txtFind').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NO_DOKUMEN", "name": "NO_DOKUMEN",
                    "render": function (data, type, full) {
                        return '<div class="text-left">' +
                            '<strong>' + full["NO_DOKUMEN"] + '</strong><br/>' +
                            '<span>' + full["NM_PERJANJIAN"] + '</span>' +
                            '</div> ';
                    }
                },
                {
                    "data": "JUDUL", "name": "JUDUL",
                    "render": function (data, type, full) {
                        return '<div class="text-left"><span>' + full["JUDUL"] + '</span></div>';
                    }
                },
                {
                    "data": "TGL_BERLAKU", "name": "TGL_BERLAKU",
                    "render": function (data, type, full) {
                        return '<span><strong>Tgl Berlaku: </strong>' + full["TGL_BERLAKU"] + '</span></br>' +
                            '<span><strong>Tgl Berakhir: </strong>' + full["TGL_BERAKHIR"] + '</span>';
                    }
                },
                {
                    "data": "ATTACHMENT", "name": "ATTACHMENT",
                    "render": function (data, type, full) {
                        return '<i class="' + ((data === '1') ? "icmn-attachment" : "") + '">';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a class="btn btn-sm btn-link" id="btn-lihat"><i class="icmn-search" aria-hidden="true"></i> Lihat Detail</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": true,
            "lengthMenu": [[3, 5, 10, 25, 100], [3, 5, 10, 25, 100]],
            "paging": true,
            "iDisplayLength": 5,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            //"info": false,
            "language": {
                "zeroRecords": "Data tidak ditemukan."
            },
            "filter": false,
            "columnDefs": [
                { "width": "150", "targets": 1 }
            ]
        });

        var table = $('#tableSearch').DataTable();
        $('#txtFind').keypress(function () {
            if (event.keyCode === 13) {
                $("#tableSearch_processing").css("visibility", "visible");
                table.ajax.reload();
            }
        });
    }






    var lihatData = function () {
        $('#tableSearch').on('click', '#btn-lihat', function (e) {
            var xTable = $('#tableSearch').dataTable();
            var nRow = $(this).parents('tr')[0];
            var x = xTable.fnGetData(nRow);

            $('#viewinputNoRec').val(x["REC_ID"]);

            $('#NomorDokumen').text(x["NO_DOKUMEN"] + ' | ' + x["JUDUL"]);
            $('#viewJnsPerjanjian').text(x["NM_PERJANJIAN"]);
            $('#viewTglPerjanjian').text(x["TGL_PERJANJIAN"]);
            $('#viewDeskripsi').text(x["DESKRIPSI"]);
            $('#viewTglBerlaku').text(x["TGL_BERLAKU"]);
            $('#viewTglBerakhir').text(x["TGL_BERAKHIR"]);

            $('#viewcreated_by').text(x["CREATED_BY"]);
            $('#viewcreated_date').text(x["CREATED_DATE"]);
            $('#viewupdated_by').text(x["UPDATED_BY"]);
            $('#viewupdated_date').text(x["UPDATED_DATE"]);

            $("#tab1").trigger('click');
            $('#modalLihatData').modal({ backdrop: 'static', keyboard: false });
            initTablePelanggan();
            initTableAttachment();
            initTablePIC();
            initTableAlur();
        });
    }

    var initTablePelanggan = function () {
        $('#viewtablePelanggan').DataTable({
            "ajax": {
                "url": "/Perjanjian/getPlg",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#viewinputNoRec').val();
                }
            },
            "columns": [
                {
                    "data": "NAMA_PEL", "name": "NAMA_PEL",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_PEL"] + '</span>';
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Pihak Terkait tidak ditemukan."
            },
            "filter": false
        });
    }

    var initTableAttachment = function () {
        $('#viewtableAttachment').DataTable({
            "ajax": {
                "url": "/Perjanjian/getAtt",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#viewinputNoRec').val();
                }
            },
            "columns": [
                {
                    "data": "FILENAME", "name": "FILENAME",
                    "render": function (data, type, full) {
                        return '<small>' + full["FILENAME"] + '</small>';
                    }
                },
                {
                    "render": function (data, type, full) {
                        var aksi = '<div class="pull-right">' +
                            '<div class="btn-group mr-2 mb-2" aria-label="" role="group">' +
                            '<a href="/FileUpload/' + full["FILENAME"] + '" target="_blank" class="btn btn-sm btn-link" id="btn-download" download><i class="icmn-download" aria-hidden="true"></i> Download</a>' +
                            '</div>' +
                            '</div>';
                        return aksi;
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Belum ada Lampiran."
            },
            "filter": false
        });
    }

    var initTablePIC = function () {
        $('#viewtablePIC').DataTable({
            "ajax": {
                "url": "/Perjanjian/getPIC",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#viewinputNoRec').val();
                }
            },
            "columns": [
                {
                    "data": "NAMA_PEG", "name": "NAMA_PEG",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_PEG"] + '</span>';
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Belum ada PIC."
            },
            "filter": false
        });
    }

    var initTableAlur = function () {
        $('#viewtableAlur').DataTable({
            "ajax": {
                "url": "/Perjanjian/getStat",
                "type": "GET",
                "data": function (data) {
                    data.XKODE_PERJANJIAN = $('#viewinputNoRec').val();
                }
            },
            "columns": [
                {
                    "render": function (data, type, full, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "data": "NAMA_ALUR", "name": "NAMA_ALUR",
                    "render": function (data, type, full) {
                        return '<span>' + full["NAMA_ALUR"] + '</span>';
                    }
                },
                {
                    "data": "REC_STAT", "name": "REC_STAT",
                    "render": function (data, type, full) {
                        return '<span class="badge badge-pill badge-' + ((data === '1') ? "success" : "info") + '">' + ((data === '1') ? "Selesai" : "Dalam Proses") + '</span><br/>' +
                            '<small class="text-muted">Updated: ' + full["CREATED_DATE"] + '</small><div class="pull-right">';
                    }
                }
            ],
            "searching": false,
            "bLengthChange": false,
            "paging": false,
            //"iDisplayLength": 20,
            "autoWidth": false,
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "info": false,
            "language": {
                "zeroRecords": "Status dokumen tidak ditemukan."
            },
            "filter": false
        });
    }



    return {
        init: function () {
            getDraft();
            getBerakhir();
            getBerlaku();
            lihatData();
        }
    };
}();

jQuery(document).ready(function () {
    Index.init();
});