﻿using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Models
{
    public class DataTableUser
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public IEnumerable<APP_USER> data { get; set; }
    }
}