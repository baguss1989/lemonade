﻿using Lemonade.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Models
{
    public class DataTablePerjanjian
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public IEnumerable<DT_PERJANJIAN> data { get; set; }
    }

    public class DataTableVwPerjanjian
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public IEnumerable<VW_DT_PERJANJIAN> data { get; set; }
    }

    public class DataTablePerjanjianPlg
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public IEnumerable<DT_PERJANJIAN_DTL_PLG> data { get; set; }
    }
}