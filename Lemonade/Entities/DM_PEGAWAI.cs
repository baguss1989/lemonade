﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class DM_PEGAWAI
    {
        public string REC_ID { get; set; }
        public string NIPP { get; set; }
        public string NAMA { get; set; }
        public string REC_STAT { get; set; }
    }
}