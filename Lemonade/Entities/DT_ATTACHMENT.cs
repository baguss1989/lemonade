﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class DT_ATTACHMENT
    {
        public string KODE_PERJANJIAN { get; set; }
        public string FILENAME { get; set; }
        public string CONTENT_TYPE { get; set; }
        public byte[] DATA { get; set; }
    }
}