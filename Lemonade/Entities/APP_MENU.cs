﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class APP_MENU
    {
        public string MENU_ID { get; set; }
        public string MENU_NAME { get; set; }
        public string MENU_CONTROLLER { get; set; }
        public string MENU_ACTION { get; set; }
        public string MENU_ICON { get; set; }
        public string MENU_ORDER_BY { get; set; }
        public string MENU_STATUS { get; set; }
        public string MENU_PARENT_ID { get; set; }
    }
}