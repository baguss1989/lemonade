﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class DT_PERJANJIAN
    {
        public string REC_ID { get; set; }
        public string NO_DOKUMEN { get; set; }
        public string JUDUL { get; set; }
        public string TGL_BERLAKU { get; set; }
        public string TGL_BERAKHIR { get; set; }
        public string DESKRIPSI { get; set; }
        public string CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public string JNS_PERJANJIAN { get; set; }
        public string NM_PERJANJIAN { get; set; }
        public string UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public string TGL_PERJANJIAN { get; set; }
    }

    public class VW_DT_PERJANJIAN
    {
        public string REC_ID { get; set; }
        public string NO_DOKUMEN { get; set; }
        public string JUDUL { get; set; }
        public string TGL_BERLAKU { get; set; }
        public string TGL_BERAKHIR { get; set; }
        public string DESKRIPSI { get; set; }
        public string CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public string JNS_PERJANJIAN { get; set; }
        public string NM_PERJANJIAN { get; set; }
        public string UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public string STATUS { get; set; }
        public string STATUS_DATE { get; set; }
        public string REC_STAT { get; set; }
        public string TGL_PERJANJIAN { get; set; }
        public string ATTACHMENT { get; set; }
        public string PIHAK_TERKAIT { get; set; }
    }

    public class DT_PERJANJIAN_DTL_PLG
    {
        public string ID { get; set; }
        public string KODE_PEL { get; set; }
        public string NAMA_PEL { get; set; }
        public string KODE_PERJANJIAN { get; set; }
    }

    public class DT_PERJANJIAN_DTL_PIC
    {
        public string ID { get; set; }
        public string KODE_PEG { get; set; }
        public string NAMA_PEG { get; set; }
        public string KODE_PERJANJIAN { get; set; }
    }

    public class DT_PERJANJIAN_DTL_UKER
    {
        public string KODE_PERJANJIAN { get; set; }
        public string KODE_UKER { get; set; }
        public string NAMA_UKER { get; set; }
    }

    public class DT_PERJANJIAN_DTL_STAT
    {
        public string ID { get; set; }
        public string KODE_PERJANJIAN { get; set; }
        public string NAMA_ALUR { get; set; }
        public string REC_STAT { get; set; }
        public string CREATED_DATE { get; set; }
        public string KODE_ALUR { get; set; }
    }

}