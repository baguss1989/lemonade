﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class APP_USER
    {
        public string ID { get; set; }
        public string USERID { get; set; }
        public string PASSWD { get; set; }
        public string NAMA { get; set; }
        public string STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
    }

    public class APP_USER_LOKAL
    {
        public string ID { get; set; }
        public string USERID { get; set; }
        public string PASSWD { get; set; }
        public string NAMA { get; set; }
        public string STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string STS { get; set; }
        public string MSG { get; set; }
    }
}