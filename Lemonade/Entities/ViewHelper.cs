﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class selectModel
    {
        public string KODE { get; set; }
        public string NAMA { get; set; }
    }

    public class dashboardModel
    {
        public string COUNT_DRAFT { get; set; }
        public string COUNT_AKTIF { get; set; }
        public string COUNT_EXP { get; set; }
        public string COUNT_ARSIP { get; set; }
        public string STS { get; set; }
        public string MSG { get; set; }
    }

    public class summaryModel
    {
        public string COUNT_KERJASAMA { get; set; }
        public string COUNT_MOU { get; set; }
        public string COUNT_PEMBORONGAN { get; set; }
        public string COUNT_PROPERTI { get; set; }
        public string STS { get; set; }
        public string MSG { get; set; }
    }

    public class outputModel
    {
        public string STS { get; set; }
        public string MSG { get; set; }
    }

    public class outputModelID
    {
        public string STS { get; set; }
        public string MSG { get; set; }
        public string REC_ID { get; set; }
    }

    public class UploadFilesResult
    {
        public string Name { get; set; }
    }

    public class ROLE_MODEL
    {
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string STS { get; set; }
        public string MSG { get; set; }
    }

    public class Counter
    {
        public string JML { get; set; }
    }

    public class EXPMONTH_MODEL
    {
        public string THNBLN { get; set; }
        public string EXP_MONTH { get; set; }
        public string JML { get; set; }
    }
}