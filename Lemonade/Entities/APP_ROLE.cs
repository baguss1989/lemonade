﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class APP_ROLE
    {
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
    }

    public class APP_USER_ROLE
    {
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string ADA { get; set; }
    }

}