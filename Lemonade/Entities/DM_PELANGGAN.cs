﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonade.Entities
{
    public class DM_PELANGGAN
    {
        public string REC_ID { get; set; }
        public string KODE_PELANGGAN { get; set; }
        public string NAMA_PELANGGAN { get; set; }
        public string ALAMAT { get; set; }
        public string KOTA { get; set; }
        public string TLP1 { get; set; }
        public string TLP2 { get; set; }
        public string EMAIL { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public string UPDATED_DATE { get; set; }
        public string REC_STAT { get; set; }
    }
}