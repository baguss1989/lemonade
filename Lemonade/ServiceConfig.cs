﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace Lemonade
{
    public class ServiceConfig
    {
        private static string[] databaseKeys = new[] { "LEMONADE" };

        public static IDbConnection GetConnection()
        {
            IDbConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings[databaseKeys[0]].ConnectionString);
            if (conn.State.Equals(ConnectionState.Closed))
                conn.Open();
            return conn;
        }
    }
}